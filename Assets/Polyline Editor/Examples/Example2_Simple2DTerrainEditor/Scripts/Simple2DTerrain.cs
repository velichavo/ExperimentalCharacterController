﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Rendering.Universal;

namespace  CustomSimple2DTerrainEditor
{
	[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer),typeof(PolygonCollider2D)), ExecuteInEditMode]
	public class Simple2DTerrain : MonoBehaviour, IEntityCollision {
		[SerializeField] ItemCollisionSurfaceType surfaceType;
		private MeshFilter meshFilter;
		private ShadowCaster2D shadowCaster;
		public List<Vector3> nodes = new List<Vector3>();
		public ItemCollisionSurfaceType SurfaceType => surfaceType;
		[SerializeField] private Mesh mesh2D;
		public Mesh Mesh2D 
		{
			get => mesh2D; 
			set => mesh2D = value;
		}

		public ShadowCaster2D ShadowCaster2DComponent
		{
			get => shadowCaster;
			set => shadowCaster = value;
		}

		private void OnEnable()
		{
			if (nodes.Count == 0)
			{
				TerrainReset();
			}
		}

		public void TerrainReset()
		{
			mesh2D = new Mesh();
			meshFilter = GetComponent<MeshFilter>();
			meshFilter.sharedMesh = mesh2D;
			nodes.Clear();
			nodes.Add(new Vector3(-1, 0, 0));
			nodes.Add(new Vector3(-1, 1, 0));
			nodes.Add(new Vector3(1, 1, 0));
			nodes.Add(new Vector3(1, 0, 0));
			shadowCaster = GetComponent<ShadowCaster2D>();
		}
    }
}
