using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ConvertAnimation))]
public class ConvertAnimationEditor : Editor
{
    private EditorCurveBinding[] targetEditorCurveBindings;
    private EditorCurveBinding[] editorCurveBindings;
    private AnimationCurve[] targetAnimationCurves;
    private AnimationCurve[] animationCurves;
    private ConvertAnimation convertAnimation;
    public override void OnInspectorGUI() 
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Convert Animation"))
        {
            convertAnimation = (ConvertAnimation)target;
            //Vector3 v = Quaternion.LookRotation(Vector3.forward, convertAnimation.testTransform.right).eulerAngles;
            //convertAnimation.AxisTransform.rotation = Quaternion.Euler(v.x, v.y, v.z + 90);
            if (!convertAnimation.clip || !convertAnimation.targetClip) return;

            editorCurveBindings = AnimationUtility.GetCurveBindings(convertAnimation.clip);
            animationCurves = new AnimationCurve[editorCurveBindings.Length];

            targetEditorCurveBindings = AnimationUtility.GetCurveBindings(convertAnimation.clip);
            targetAnimationCurves = new AnimationCurve[editorCurveBindings.Length];

            for(var i = 0; i < animationCurves.Length; i ++)
            {
                animationCurves[i] = AnimationUtility.GetEditorCurve(convertAnimation.clip, editorCurveBindings[i]);
                targetAnimationCurves[i] = AnimationUtility.GetEditorCurve(convertAnimation.clip, editorCurveBindings[i]);
                //targetAnimationCurves[i].SmoothTangents(i, 0);
                //Debug.Log(editorCurveBindings[i].propertyName);
            }
            Convert();
            for(var i = 0; i < animationCurves.Length; i ++)
            {
                AnimationUtility.SetEditorCurve(convertAnimation.targetClip, targetEditorCurveBindings[i], targetAnimationCurves[i]);
            }
            //animationUtility.SetEditorCurves(anyStateAnimator.clip, editorCurveBindings, animationCurves);
            Debug.Log("Done");
        }
    }
    private void Convert()
    {
        int index = 0;
        int total = 0;
        int bone = 0;
        while(index < editorCurveBindings.Length)
        {
            switch(editorCurveBindings[index].propertyName)
            {
                case "m_LocalPosition.x":
                    Debug.Log("Position");
                    SetCurveAllBonesPosition(index, bone);
                    index += 3;
                    total ++;
                    break;
                case "m_LocalRotation.x":
                    Debug.Log("Rotation");
                    SetCurveAllBones(index, bone);
                    index += 4;
                    total ++;
                    bone ++;
                    break;
                case "m_LocalScale.x":
                    Debug.Log("Scale");
                    index += 3;
                    break;
            }
        }
        Debug.Log($"Total: {total}");
    }

    private void SetCurveAllBones(int index, int bone)
    {
        for(var i = 0; i < animationCurves[index].keys.Length; i++)
        {
            Quaternion qq = new Quaternion(animationCurves[index].keys[i].value, animationCurves[index + 1].keys[i].value, animationCurves[index + 2].keys[i].value, animationCurves[index + 3].keys[i].value);
            Vector3 v = Vector3.zero;
            switch (editorCurveBindings[index].path) 
            {
                case "lower_body_bone/upper_body_bone/shoulder_left_bone":
                    convertAnimation.SourceBoneTransform[bone].localRotation = Quaternion.Euler(120f, -9f, 85f) * qq;
                    break;
                case "lower_body_bone/upper_body_bone/shoulder_right_bone":
                    convertAnimation.SourceBoneTransform[bone].localRotation = Quaternion.Euler(120f, -9f, -85f) * qq;
                    break;
                default:
                    convertAnimation.SourceBoneTransform[bone].localRotation = qq;
                    break;
            }
            v = convertAnimation.SourceBoneTransform[bone].up;

            v = Vector3.ProjectOnPlane(v, Vector3.left);
            v = new Vector3(v.z, v.y, 0);
            //if (i == animationCurves[index].keys.Length - 1) convertAnimation.intermediateBones[bone] = v;
            Quaternion q = Quaternion.LookRotation(Vector3.up, v); 
            if (i == 0)  Debug.Log($"Vector KeyFrame[{i}]]: {v}, Euler: {q.eulerAngles}");

            Transform parent = convertAnimation.targetBone[bone].parent;
            convertAnimation.targetBone[bone].SetParent(null);
            
            convertAnimation.targetBone[bone].right = v;
            convertAnimation.targetBone[bone].SetParent(parent);

            SetAnimationCurve(convertAnimation.targetBone[bone].localRotation, index, i);
        }
    }

    public void SetCurveAllBonesPosition(int index, int bone)
    {
        for(var i = 0; i < animationCurves[index].keys.Length; i++)
        {
            Vector3 v = new Vector3(animationCurves[index].keys[i].value, animationCurves[index + 1].keys[i].value, animationCurves[index + 2].keys[i].value);
            SetAnimationCurve(new Vector3(v.z - .3f, v.y - .75f, 0), index, i);
        }
    }

    public void CalculateRotation(int index)
    {

        //Debug.Log($"Length: {animationCurves[index].keys.Length}");
        for(var i = 0; i < animationCurves[index].keys.Length; i++)
        {
            convertAnimation.AxisTransform.rotation = new Quaternion(animationCurves[index].keys[i].value, animationCurves[index + 1].keys[i].value, animationCurves[index + 2].keys[i].value, animationCurves[index + 3].keys[i].value);
            Vector3 v = convertAnimation.AxisTransform.up;
            v = Vector3.ProjectOnPlane(v, Vector3.right);
            v = new Vector3(v.z, v.y, 0);
            Quaternion q = Quaternion.LookRotation(Vector3.forward, v); 

            SetAnimationCurve(q, index, i);
        }
    }

    private void SetAnimationCurve(Quaternion q, int curveIndex, int keyIndex)
    {
        //Debug.Log($"Q: {q}, curveIndex: {curveIndex}, keyIndex: {keyIndex}");
        //Debug.Log($"targetAnimationCurves: {targetAnimationCurves}, animationCurves: {animationCurves},  key: {animationCurves[curveIndex].keys[keyIndex]}");
        float f = 0;
        for (var i = 0; i < 4; i++)
        {
            Keyframe keyframe = new Keyframe(animationCurves[curveIndex + i].keys[keyIndex].time, q[i], f, f, f, f);
            keyframe.weightedMode = WeightedMode.None;
            targetAnimationCurves[curveIndex + i].MoveKey(keyIndex, keyframe);
        }
    }
    private void SetAnimationCurve(Vector3 v, int curveIndex, int keyIndex)
    {
        //Debug.Log($"Q: {q}, curveIndex: {curveIndex}, keyIndex: {keyIndex}");
        //Debug.Log($"targetAnimationCurves: {targetAnimationCurves}, animationCurves: {animationCurves},  key: {animationCurves[curveIndex].keys[keyIndex]}");
        float f = 0;
        for (var i = 0; i < 3; i++)
        {
            Keyframe keyframe = new Keyframe(animationCurves[curveIndex + i].keys[keyIndex].time, v[i], f, f, f, f);
            keyframe.weightedMode = WeightedMode.None;
            targetAnimationCurves[curveIndex + i].MoveKey(keyIndex, keyframe);
        }
    }

}
