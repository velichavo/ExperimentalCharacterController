using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicEnviroment : MonoBehaviour, IEntityCollisionAction
{
    [SerializeField] ItemCollisionSurfaceType surfaceType;
    private Rigidbody2D rb2d;
    public ItemCollisionSurfaceType SurfaceType => surfaceType;
    public GameObject CharacterParent{get; private set;}

    internal void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    public void OnBulletCollision(BaseBullet bullet)
    {
        rb2d.AddForce(bullet.speed * bullet.direction / 100, ForceMode2D.Impulse);
    }

    public bool OnItemCollision(IItem item)
    {
        rb2d.AddForce(item.ItemComponents.Rb2D.velocity / 10, ForceMode2D.Impulse);
        return true;
    }

    public void OnItemExplosion(BaseExplosion explosion)
    {
        
    }

    void IEntityCollisionAction.ApplyForceEntity(BaseExplosion explosion)
    {
        throw new System.NotImplementedException();
    }
}
