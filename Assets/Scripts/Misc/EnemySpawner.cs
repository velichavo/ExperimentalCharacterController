using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] int count;
    [SerializeField] Transform parent;
    [SerializeField] Enemy enemy;

    void Start()
    {
        for(var i = 0; i < count; i++)
        {
            Enemy e = Instantiate<Enemy>(enemy, transform.position + i * Vector3.right * .5f, Quaternion.identity, parent);
        }
    }


}
