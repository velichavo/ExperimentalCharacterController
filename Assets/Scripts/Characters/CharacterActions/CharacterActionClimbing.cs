using System;
using UnityEngine;


[Serializable]
public class CharacterActionClimbing
{
    public const float MaxDistanceRaycast = .3f;
    [SerializeField] private float groundOffset = .7f;
    [SerializeField] private float stepHeightObstacle = .5f;
    [SerializeField] private float heightBetweenCheck = .6f;
    [SerializeField] private Vector2 offsetClimbingHigh;
    [SerializeField] private Vector2 offsetClimbingMiddle;
    [SerializeField] private Vector2 offsetClimbingLow;
    [SerializeField] private Vector2 offsetAnimationHigh;
    [SerializeField] private Vector2 offsetAnimationMiddle;
    [SerializeField] private Vector2 offsetAnimationLow;
    [SerializeField] private Transform DebugColliderViewer;
    private Character character;
    private RaycastHit2D raycastHitClimbing;
    private Vector2 climbingPosition;

    public bool IsClimbing {get; set;}

    public void Init(Character newCharacter)
    {
        character = newCharacter;
    }

    public void TryCheckClimbing()
    {
        if(character.Actions.IsGrounded) return;
        //CheckClimbingLow();
        CheckClimbingMiddle();
        CheckClimbingHigh();
    }
    private void CheckClimbingHigh()
    {
        ApplyClimbing(offsetClimbingHigh.y, offsetAnimationHigh, AnimationVariables.body_climbing, AnimationVariables.legs_climbing);
    }
    private void CheckClimbingMiddle()
    {
        ApplyClimbing(offsetClimbingMiddle.y, offsetAnimationMiddle, AnimationVariables.body_climbing_middle, AnimationVariables.legs_climbing_middle);
    }
    private void CheckClimbingLow()
    {
        ApplyClimbing(offsetClimbingLow.y, offsetAnimationLow, AnimationVariables.body_climbing_middle, AnimationVariables.legs_climbing_middle);
    }

    private void ApplyClimbing(float offset, Vector2 offsetAnimationClimbing, AnimationVariables bodyAnimation, AnimationVariables legsAnimation)
    {
        if (!character.Actions.IsGrounded && !IsClimbing)
        {
            //проверка столкновения и расчет расстояния до запрыгиваемой поверхности круглым колидером размером с ширину капсулы
            raycastHitClimbing = CapsulecastClimbing(new Vector2(character.Components.Capsule2D.bounds.size.x * character.transform.localScale.x,
                groundOffset + heightBetweenCheck + offset * stepHeightObstacle + character.Components.Capsule2D.bounds.extents.x - character.Components.Capsule2D.bounds.extents.y), Vector2.down,
                new Vector2(character.Components.Capsule2D.bounds.size.x, character.Components.Capsule2D.bounds.size.x), MaxDistanceRaycast);

            if(raycastHitClimbing.distance > 0 && raycastHitClimbing.distance < MaxDistanceRaycast)
            {
                //расчитывается расстояние до уступа по х и проверяется пространство на головой
                RaycastHit2D rx = CapsulecastClimbing(new Vector2(0, character.Components.Capsule2D.bounds.extents.y), Vector2.right  * character.transform.localScale.x,
                    character.Components.Capsule2D.bounds.size, MaxDistanceRaycast);
                
                Vector2 newOffset = new Vector2(character.Components.Capsule2D.bounds.size.x * character.transform.localScale.x, 
                    groundOffset + heightBetweenCheck + offset * stepHeightObstacle - (raycastHitClimbing.distance - CharacterStats.shellRadius));

                //финальная проверка, что в точке запрыгивания нет колидера
                RaycastHit2D ry = CapsulecastClimbing(newOffset, Vector2.zero, character.Components.Capsule2D.bounds.size);
                // если в точке ry есть колидер и дистация rx = 0(значит, что над головой колидер), то не запрыгиваем
                if (!ry.collider && rx.distance != 0)
                {
                    character.Components.CaptureTarget.position = new Vector3(rx.point.x, raycastHitClimbing.point.y, 0);
                    climbingPosition = character.Components.Rb2D.position + newOffset;
                    float offsetAnimation = character.Components.Rb2D.position.y + offsetAnimationClimbing.y - character.Components.CaptureTarget.position.y;
                    character.Components.Rb2D.position = (new Vector2(character.Components.Rb2D.position.x + rx.distance - CharacterStats.shellRadius, character.Components.Rb2D.position.y - offsetAnimation));
                    IsClimbing = true;
                    character.Actions.Velocity = Vector2.zero;
                    //отключить инверсную кинематику для головы и верхней чсти туловища
                    character.ObjectReferences.CharacterIKManager2D.solvers[0].enabled = false;
                    character.ObjectReferences.CharacterIKManager2D.solvers[1].enabled = false;
                    character.Components.CharacterAnimator.TryPlayAnimation(bodyAnimation);
                    character.Components.CharacterAnimator.TryPlayAnimation(legsAnimation);
                }
            }
        }
    }

    private RaycastHit2D RaycastClimbing(Vector2 offset, Vector2 direction)
    {
        return Physics2D.Raycast((Vector2)character.Components.Rb2D.position + offset, direction, MaxDistanceRaycast, character.Components.CharacterCollisionFilter.layerMask);
    }
    private RaycastHit2D CapsulecastClimbing(Vector2 offset, Vector2 direction, Vector2 size, float distance = 0)
    {
        return Physics2D.CapsuleCast((Vector2)character.Components.Rb2D.position + offset, size, CapsuleDirection2D.Vertical, 0, direction, distance, character.Components.CharacterCollisionFilter.layerMask);
    }
        public void EndClimbing()
    {
        //вызывается из анимации legs_climbing
        character.ObjectReferences.CharacterIKManager2D.solvers[0].enabled = true;
        character.ObjectReferences.CharacterIKManager2D.solvers[1].enabled = true;
        character.Actions.Movement = climbingPosition;
        IsClimbing = false;
    }
}
