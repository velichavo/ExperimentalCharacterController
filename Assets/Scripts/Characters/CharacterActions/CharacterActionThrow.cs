using UnityEngine;
using WeaponCharacter;

public enum CharacterThrowState {none, Ready, Swing, Throw}
public class CharacterActionThrow
{
    private Character character;
    public CharacterThrowState ThrowState {get; set;}

    public void Init(Character newCharacter)
    {
        character = newCharacter;
        ThrowState = CharacterThrowState.Ready;
    }

    public void Swing(GetKeyState keyState)
    {
        switch(ThrowState)
        {
            case CharacterThrowState.Ready:
                ThrowState = CharacterThrowState.Swing;
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_throw_item_begin);
                break;
            case CharacterThrowState.Throw:
                if (keyState == GetKeyState.GetKeyUp)
                {
                    character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_throw_item_end);
                }
                break;
            case CharacterThrowState.Swing:
                if (keyState == GetKeyState.GetKeyUp)
                {
                    ThrowState = CharacterThrowState.Ready;
                    character.Components.CharacterAnimator.OnAnimationEnd(character.Components.CharacterAnimator.CurrentAnimationBody);
                }
                break;
        }
    }

    public void EndSwing()
    {
        ThrowState = ThrowState == CharacterThrowState.Swing ? ThrowState = CharacterThrowState.Throw : ThrowState = CharacterThrowState.Ready;
    }

    public void EndThrowing()
    {
        ThrowState = CharacterThrowState.Ready;
        character.Components.CharacterAnimator.OnAnimationEnd(character.Components.CharacterAnimator.CurrentAnimationBody);
        BaseItem item = character.ObjectReferences.LastEquipItem as BaseItem;
        if (item != null)
        {
            IInventoryItem inventoryItem = character.ObjectReferences.CurrentInventory.GetItem(item.ItemComponents.inventoryItem.inventoryItemInfo);
            if (inventoryItem != null)
                inventoryItem.Equip(true);  
        }
    }
}