using UnityEngine;

public class NewPlayer : Character
{
    [SerializeField] private CharacterUtilities utilities = new ();
    private static NewPlayer characterInstance;
    public CharacterUtilities Utilities => utilities;
    public static NewPlayer CharacterInstance
    {
        get
        {
            if (characterInstance == null) characterInstance = GameObject.FindObjectOfType<NewPlayer>();
            return characterInstance;
        }
    }
    public override void Awake()
    {
        base.Awake();
        utilities.Init(this);
    }
    public override void Update() 
    {
        actions.SetFollowPosition(CameraController.WorldMousePos);
        base.Update();
        if(stats.CharacterDead) return;
        utilities.HandleInput();
        utilities.SetTargetPosition();
    }
}