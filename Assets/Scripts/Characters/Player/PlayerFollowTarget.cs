using UnityEngine;

public class PlayerFollowTarget : MonoBehaviour
{
    [SerializeField, Range(.01f, 1)] private float radius = .1f;
    [SerializeField] private LayerMask layerMask;
    private IInfoViewerble current;
    private Character character;

    public void Init(Character newCharacter)
    {
        character = newCharacter;
    }
    private void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, radius, Vector2.right, 0, layerMask);
        if (hit)
        {
            IInfoViewerble[] iv = hit.transform.GetComponentsInParent<IInfoViewerble>();
            if (iv.Length > 0)
            {
                if(!object.ReferenceEquals(iv[0], current))
                {
                    if(current != null)
                        current.ViewInfo(false);
                    iv[0].ViewInfo(true);
                    current = iv[0];
                }
                else
                {
                    iv[0].ViewInfo(true);
                    current = iv[0];
                }
            }
        }
        else
            if (current != null)
            {
                current.ViewInfo(false);
                current = null;
            }
    }

    public void TakeActionOnItem()
    {
        if (current != null)
        {
            current.TakeActionOnItem(character);
        }
    }

    public void TargetSetPosition(Vector3 position)
    {
        transform.localPosition = position;
    }
}