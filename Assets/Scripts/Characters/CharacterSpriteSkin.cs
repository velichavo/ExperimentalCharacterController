using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;

public class CharacterSpriteSkin : MonoBehaviour
{
    private bool isDrawSprites;
    private Character character;
    private SpriteSkinItem[] spriteBodyPart;
    private SpriteSkinItem[] spriteOutfit;
    public void Init(Character newCharacter)
    {
        this.character = newCharacter;
        SpriteSkin[] item = GetComponentsInChildren<SpriteSkin>();
        List<SpriteSkinItem> bodyPart = new List<SpriteSkinItem>();
        List<SpriteSkinItem> outfit = new List<SpriteSkinItem>();
        for(var i = 0; i < item.Length; i++)
        {
            SpriteRenderer sr = item[i].GetComponent<SpriteRenderer>();
            Outfit fit = item[i].GetComponent<Outfit>();
            if(fit)
                outfit.Add(new SpriteSkinItem(item[i], sr, fit.IsDrawSprite));
            else
                bodyPart.Add(new SpriteSkinItem(item[i], sr, true));
        }
        spriteBodyPart = bodyPart.ToArray();
        spriteOutfit = outfit.ToArray();
    }

    public void SetEnabledSpriteSkinItem(bool enabled)
    {
        if(isDrawSprites == enabled) return;
        isDrawSprites = enabled;
        //character.Components.CharacterAnimator.SetEnabled(enabled);
        character.Components.CharacterIKManager.gameObject.SetActive(enabled);
        spriteBodyPart[0].SetEnabled(enabled);
    }
}
