public interface ICharacter
{
    public CharacterComponents Components { get;}
    public CharacterObjectReferences ObjectReferences { get;}
    CharacterStats Stats { get;}
    public CharacterActions Actions { get;}
}