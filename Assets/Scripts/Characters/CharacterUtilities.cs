using System;
using System.Collections.Generic;
using UnityEngine;
using WeaponCharacter;

[Serializable]
public class CharacterUtilities
{
    private Character character;
    [SerializeField] UIController uIController;
    [SerializeField] PlayerFollowTarget followTarget;

    private string handleInput = "Horizontal";
    private List<Command> commands = new List<Command>();
    //Меняется в UIController
    public UIController PlayerUIController => uIController;
    public PlayerFollowTarget FollowTarget => followTarget;
    public void Init(Character newCharacter)
    {
        character = newCharacter;
        commands.Add(new JumpCommand(character, KeyCode.Space));
        commands.Add(new AttackCommand(character, KeyCode.Mouse0));
        commands.Add(new TakeAction(character, KeyCode.E));
        commands.Add(new ToggleInventory(character, KeyCode.I, true));
        commands.Add(new ReloadWeaponCommands(character, KeyCode.R));
        commands.Add(new ThrowOutHandCommand(character, KeyCode.H));
        uIController.Init(character);
        followTarget.Init(character);
        ChangeTextUIHealhPoint(character);
        ChangeTextUIClip(-1);
        ChangeTextUIAmmo(-1);
        character.Ability.OnChangeHealthPoint += ChangeTextUIHealhPoint;
    }

    public void ChangeTextUIHealhPoint(Character player)
    {
        uIController.SetTextHealthPoint(player.Ability.HealthPoint);
    }
    public void ChangeTextUIClip(int value)
    {
        uIController.SetTextClip(value);
    }

    public void ChangeTextUIAmmo(int value)
    {
        uIController.SetTextAmmo(value);
    }

    public void SetTargetPosition()
    {
        followTarget.TargetSetPosition((CameraController.WorldMousePos - character.transform.position) * Vector2.one * character.transform.localScale);
    }
    public void HandleInput()
    {
        character.Actions.Walk(Input.GetAxisRaw(handleInput));

        foreach (Command command in commands)
        {
            if(!uIController.IsShowInventory || command.ActiveInInterface)
            {
                if(Input.GetKeyDown(command.Key))
                {
                    command.GetKeyDown();
                }
                if(Input.GetKeyUp(command.Key))
                {
                    command.GetKeyUp();
                }
                if(Input.GetKey(command.Key))
                {
                    command.GetKey();
                }
            }
        }

        if (Input.GetAxisRaw("Vertical") < 0)
        {
            character.Actions.JumpOff();
        }
    }
}
