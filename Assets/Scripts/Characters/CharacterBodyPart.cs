using System.Collections.Generic;
using UnityEngine;

public class CharacterBodyPart : MonoBehaviour
{
    [SerializeField] private List<BodyPartItem> bodyPart;

    public void Init()
    {
        CalculateBodyPart();
        SetEnabled(false);
    }

    private void CalculateBodyPart()
    {
        bodyPart = new();
        Rigidbody2D[] childrens;
        childrens = GetComponentsInChildren<Rigidbody2D>();
        for(var i = 0; i < childrens.Length; i++)
        {
            Rigidbody2D parent = childrens[i].transform.GetComponentsInParent<Rigidbody2D>()[1];
            if (i == 0) parent = childrens[i];
            bodyPart.Add(childrens[i].GetComponent<BodyPartItem>().Init(childrens[i], parent));
        }
    }

    public void SetEnabled(bool enabled)
    {
        for(var i = 0; i < bodyPart.Count; i++)
            bodyPart[i].SetEnabled(enabled);
    }
}
