using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.U2D.IK;

[System.Serializable]
public class CharacterComponents
{

    [SerializeField] private Rigidbody2D rb2D;
    [SerializeField] private CapsuleCollider2D capsule2D;
    [SerializeField] private Transform handWithWeapon;
    [SerializeField] private Transform captureTarget;
    [SerializeField] private LimbSolver2D gripSolver;
    [SerializeField] private LimbSolver2D handguardSolver;
    [SerializeField] private CharacterBodyPart bodyPart;
    [SerializeField] private CharacterSpriteSkin spriteSkin;
    [SerializeField] private bool useContactFilter = false;
    [SerializeField] private ContactFilter2D characterCollisionFilter;
    [SerializeField] private ContactFilter2D itemCollisionFilter;

    private AnyStateAnimator characterAnimator;
    private Character character;


    public Transform HandWithWeapon => handWithWeapon;
    public Transform CaptureTarget => captureTarget;
    public LimbSolver2D GripSolver => gripSolver;
    public LimbSolver2D HandguardSolver => handguardSolver;
    public CapsuleCollider2D Capsule2D => capsule2D;
    public Rigidbody2D Rb2D => rb2D;
    public AnyStateAnimator CharacterAnimator => characterAnimator;
    public ContactFilter2D CharacterCollisionFilter => characterCollisionFilter;
    public ContactFilter2D ItemCollisionFilter => itemCollisionFilter;
    public CharacterBodyPart CharacterBodyPart => bodyPart;
    public CharacterSpriteSkin CharacterSpriteSkin => spriteSkin;
    public Transform CurrentChainRightHand {get; set;}
    public Transform CurrentChainLeftHand {get; set;}
    public IKChain2D ChainRightHand{get; set;}
    public IKChain2D ChainLeftHand{get; set;}
    public IKManager2D CharacterIKManager{get; set;}
    
    public void Init(Character newCharacter)
    {
        character = newCharacter;
        rb2D = newCharacter.GetComponent<Rigidbody2D> ();
        capsule2D = newCharacter.GetComponent<CapsuleCollider2D>();
        characterAnimator = newCharacter.GetComponentInChildren<AnyStateAnimator>();
        characterAnimator.Init(newCharacter);
        spriteSkin = newCharacter.GetComponentInChildren<CharacterSpriteSkin>();
        CharacterIKManager = newCharacter.GetComponentInChildren<IKManager2D>();
        spriteSkin.Init(newCharacter);
        if(!useContactFilter)
        {
            characterCollisionFilter.useTriggers = false;
            characterCollisionFilter.SetLayerMask(Physics2D.GetLayerCollisionMask (newCharacter.gameObject.layer));
            characterCollisionFilter.useLayerMask = true;
        }
        if(CaptureTarget) CaptureTarget.SetParent(null);
        if(gripSolver) ChainRightHand = character.Components.GripSolver.GetChain(0);
        if(handguardSolver) ChainLeftHand = character.Components.handguardSolver.GetChain(0);
        captureTarget.gameObject.SetActive(false);
        
    }
    
    public void ToggleDrawSprite()
    {
        spriteSkin.SetEnabledSpriteSkinItem(character.ObjectReferences.MainCameraController.IsRectCamera(character.transform.position));
    }
}
