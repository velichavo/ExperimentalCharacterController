using UnityEngine;

[System.Serializable]
public class CharacterStats
{
    Character character;
    public const float minMoveDistance = 0.001f;
    public const float shellRadius = 0.01f;
    
    [SerializeField] bool characterDead = false;
    [SerializeField, Range(0, 1)] private float minGroundNormalY;
    [SerializeField] private float gravityModifier;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float jumpTakeOffSpeed;
    [SerializeField] private float speedJumpOff;
    [SerializeField] private float accelerationOnGround;
    [SerializeField] private float accelerationOnAir;
    [SerializeField] private float dampingOnGround;
    [SerializeField] private float dampingOnAir;
    public bool CharacterDead {get => characterDead;}
    public float MinGroundNormalY { get{return minGroundNormalY;} }
    public float GravityModifier { get{return gravityModifier;} }
    public float MaxSpeed { get{return maxSpeed;} }
    public float JumpTakeOffSpeed { get{return jumpTakeOffSpeed;} }
    public float AccelerationOnGround { get{return accelerationOnGround;} }
    public float DampingOnGround { get{return dampingOnGround;} }
    public float DampingOnAir { get{return dampingOnAir;} }
    public float AccelerationOnAir { get{return accelerationOnAir;} }
    public float SpeedJumpOff { get{return speedJumpOff;} }

    public void Init(Character newCharacter)
    {
        character = newCharacter;
    }

    public void SetCharacterDead(bool enabled)
    {
        characterDead = enabled;
    }
}
