using UnityEngine;
using UnityEngine.U2D.Animation;

public class SpriteSkinItem
{
    private bool isDrawSprite = true;
    private SpriteSkin spriteSkin;
    private SpriteRenderer spriteRenderer;

    public GameObject gameObject => spriteRenderer.gameObject;
    public SpriteSkinItem(SpriteSkin spriteSkin, SpriteRenderer spriteRenderer, bool enabled)
    {
        this.spriteSkin = spriteSkin;
        this.spriteRenderer = spriteRenderer;
        if (!isDrawSprite)
            spriteRenderer.gameObject.SetActive(false);
        this.isDrawSprite = enabled;
    }

    public void SetEnabled(bool enabled)
    {
        if (!isDrawSprite) return;
        spriteRenderer.gameObject.SetActive(enabled);
    }
}