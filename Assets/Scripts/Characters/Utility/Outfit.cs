using UnityEngine;

public class Outfit : MonoBehaviour
{
    [SerializeField] private bool isDrawSprite;

    public bool IsDrawSprite => isDrawSprite;
}
