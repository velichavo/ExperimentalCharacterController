using UnityEngine;

[System.Serializable]
public class BodyPartItem : MonoBehaviour, IEntityCollisionAction
{
    [SerializeField] bool enabledCollisionAlive = true;
    [SerializeField] bool isBreakJoint = true;
    [SerializeField] float multiplierDamage = 1f;
    [SerializeField] int breakJointHP = -100;

    private ItemCollisionSurfaceType surfaceType = ItemCollisionSurfaceType.Charecter;
    private Rigidbody2D rigidbody2Dpart;
    private Collider2D collider2Dpart;
    private HingeJoint2D hingeJoint2Dpart;
    private Rigidbody2D parent;
    private Character character;
    public ItemCollisionSurfaceType SurfaceType => surfaceType;
    public GameObject CharacterParent{get; private set;}

    public BodyPartItem Init(Rigidbody2D rigidbody2D, Rigidbody2D parent)
    {
        this.rigidbody2Dpart = rigidbody2D;
        this.parent = parent;
        collider2Dpart = rigidbody2D.GetComponent<Collider2D>();
        hingeJoint2Dpart = rigidbody2D.GetComponent<HingeJoint2D>();
        character = GetComponentsInParent<Character>()[0];
        CharacterParent = character.gameObject;
        return this;
    }

    public void SetEnabled(bool enabled)
    {
        rigidbody2Dpart.bodyType = enabled ? RigidbodyType2D.Dynamic : RigidbodyType2D.Static;
        if (hingeJoint2Dpart) hingeJoint2Dpart.enabled = enabled;
    }

    public void OnBulletCollision(BaseBullet bullet)
    {
        if (!object.ReferenceEquals(bullet.parent.gameObject, character.transform.gameObject))
            character.Ability.ApplyDamage(bullet.damage * multiplierDamage);
    }

    public bool OnItemCollision(IItem item)
    {
        if (!object.ReferenceEquals(item.ItemComponents.character, character))
        {
            character.Actions.Die(true);
            item.ItemComponents.character = null;
            item.ItemComponents.Manager.OnThrowing -= item.ItemMovement.UpdateMovement;
            if(hingeJoint2Dpart) hingeJoint2Dpart.enabled = false;
            rigidbody2Dpart.AddForce(item.ItemComponents.Rb2D.velocity / 10, ForceMode2D.Impulse);
            if (parent) parent.AddForce(item.ItemComponents.Rb2D.velocity / 10, ForceMode2D.Impulse);
            transform.SetParent(character.Components.Rb2D.transform);
            return true;
        }
        return false;
    }

    public void OnItemExplosion(BaseExplosion explosion)
    {
        Vector2 vector = transform.position - explosion.transform.position;
        float power = (explosion.damageRadius - vector.magnitude) / explosion.damageRadius;
        character.Ability.ApplyDamage(explosion.damage * multiplierDamage * power);
    }

    public void ApplyForceEntity(BaseExplosion explosion)
    {
        Vector2 vector = transform.position - explosion.transform.position;
        float power = (explosion.damageRadius - vector.magnitude) / explosion.damageRadius;
        Vector2 force = power * vector;

        if(character.Ability.HealthPoint < breakJointHP)
        {
            if(hingeJoint2Dpart && isBreakJoint)
            {
                transform.SetParent(character.Components.Rb2D.transform);
                hingeJoint2Dpart.enabled = false;
            }
        }
        if(character.Stats.CharacterDead)
        {
            rigidbody2Dpart.AddForce(force / 2, ForceMode2D.Impulse);
        }
        else
        {
            character.Actions.CharacterAddForce(force / 2);
        }
    }
}