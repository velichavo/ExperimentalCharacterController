using UnityEngine;
using TMPro;

public class CharacterInfoViewer : MonoBehaviour
{
    [SerializeField] Vector3 offset;
    [SerializeField] TextMeshProUGUI textInfo;
       
    public TextMeshProUGUI TextInfo => textInfo;
    public Vector3 OffsetPositionText => offset;

    public void SetViewerText(Character character, bool enabled)
    {
        gameObject.SetActive(enabled);
        TextInfo.text = $"Health: {character.Ability.HealthPoint}\n";
        TextInfo.text += character.ObjectReferences.currentWeapon != null ? $"Weapon: {character.ObjectReferences.currentWeapon.ItemComponents.InventoryItemInfo.title}" : "Weapon: -";
        TextInfo.transform.position = character.ObjectReferences.MainCameraController.WorldToScreenPoint(character.transform.position + character.ObjectReferences.CharacterInfoViewer.OffsetPositionText);
    }
}
