using System;
using UnityEngine;

[System.Serializable]
public class CharacterAbility
{
    public event Action<Character> OnChangeHealthPoint;
    private readonly float coefLevel = 10000f;
    protected Character character;
    [SerializeField] protected int healthPoint;
    [SerializeField] private int maxWeight;
    [SerializeField] private int experiencePoint;

    public int Level => (int)(experiencePoint / coefLevel + 1);
    public int HealthPoint => healthPoint;

    public void Init(Character newCharacter)
    {
        character = newCharacter;
    }
    public virtual void ApplyDamage(float damage)
    {
        healthPoint -= (int)damage;
        if (healthPoint <= 0 && !character.Stats.CharacterDead) character.Actions.Die(true);
        OnChangeHealthPoint?.Invoke(character);
    }
    public int GetAmountExperienceToNextLevel()
    {
        return (int)(Level * coefLevel);
    }
}