using System.Collections.Generic;
using System;
using UnityEngine; 
using UnityEngine.U2D.IK;
using WeaponCharacter;
public class CharacterActions
{
    Character character;
    private bool jumpOff = false;
    private bool isJumping = false;
    private Vector2 velocity;
    private Vector2 groundNormal = Vector2.up;
    public Vector3 followTarget {get; private set;}
    private float isMoveX;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D> (16);

    public bool IsGrounded {get; set;}
    public Vector2 Velocity {get {return velocity;} set {velocity = value;}}
    public Vector2 Movement {get; set;}
    public void Init(Character newCharacter)
    {
        character = newCharacter;
        Movement = character.Components.Rb2D.position;
    }
    public void Walk(float value)
    {
        isMoveX = value;
        if (isJumping || !IsGrounded || character.ActionClimbing.IsClimbing ) return;
        if (value != 0)
        {
            if(((followTarget.x - character.transform.position.x) * value) > 0)
            {
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_walk_forward);
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.legs_walk_forward);
            }
            else 
            {
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_walk_back);
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.legs_walk_back);
            }
        }
        else
        {
            character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_idle);
            character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.legs_idle);
        }
    }
    public void Jump()
    {
        velocity.y = character.Stats.JumpTakeOffSpeed;
        isJumping = false;
        IsGrounded = false;
    }
    public void SetCaptureTarget(bool enabled)
    {
        character.Components.ChainRightHand.target = enabled ? character.Components.CaptureTarget : character.Components.CurrentChainRightHand ? character.Components.CurrentChainRightHand : null;
        character.Components.ChainLeftHand.target = enabled ? character.Components.CaptureTarget : character.Components.CurrentChainLeftHand ? character.Components.CurrentChainLeftHand : null;
    }

    public void BeginJump()
    {
        if (IsGrounded) 
        {
            if(velocity.magnitude > CharacterStats.minMoveDistance)
            {
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_jump_forward);
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.legs_jump_forward);
                Jump();
            }
            else
            {
                isJumping = true;
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.body_jump);
                character.Components.CharacterAnimator.TryPlayAnimation(AnimationVariables.legs_jump);
            }
        }
    }
    public void JumpOff()
    {
        if(IsGrounded)
        {
            jumpOff = true;
            velocity.y = -character.Stats.SpeedJumpOff;
        }
    }   
    public void Attack(GetKeyState state)
    {
        if (character.ObjectReferences.currentWeapon == null) return;
        character.ObjectReferences.currentWeapon.ItemActions.Attack(character, state);
    }

    public void CheckCollisionItem()
    {
        RaycastHit2D[] result = new RaycastHit2D[1];
        character.Components.Rb2D.Cast(Vector2.right, character.Components.ItemCollisionFilter, result, 0);
        if(result[0].collider != null)
            {
                IItem item = result[0].transform.GetComponent<IItem>();
                if (item != null)
                {
                    item.ItemComponents.Manager.OnThrowing -= item.ItemMovement.UpdateMovement;
                    AddToInventory(character, item);
                }
                else
                    throw new System.Exception("Неизвестный тип");
            }
    }

    public void AddToInventory(Character character, IItem item)
    {
        item.ItemActions.SetItemEnabled(false);
        character.ObjectReferences.CurrentInventory.TryToAdd(character, item.ItemComponents.inventoryItem);
    }

    public void Equip(IItem item, bool isEquipped)
    {
        if (character.ObjectReferences.currentWeapon != null)
        {
            if (isEquipped)
                character.ObjectReferences.currentWeapon.ItemComponents.inventoryItem.inventoryItemState.toggle.isOn = false;
        }
        character.ObjectReferences.currentWeapon = item.ItemActions.Equip(character, isEquipped);
        if(character.ObjectReferences.currentWeapon != null)
            character.ObjectReferences.LastEquipItem = character.ObjectReferences.currentWeapon;
    }
    public void ThrowOutHand()
    {
        if (character.ObjectReferences.currentWeapon != null)
        {   
            IItem item = character.ObjectReferences.currentWeapon;
            
        }
    }

    public void ReloadWeapon()
    {
        if (character.ObjectReferences.currentWeapon != null)
        {
            ((WeaponActions)character.ObjectReferences.currentWeapon.ItemActions).ReloadWeapon();
        }
    }
    public void SetFollowPosition(Vector3 target)
    {
        followTarget = target;
    }
    
    public void BodyRotateToAim()
    {    
        if (character.ActionClimbing.IsClimbing) return;
        Vector3 directon = new Vector3(followTarget.x, followTarget.y, 0) - character.Components.HandWithWeapon.transform.position;
        character.transform.localScale = new Vector3(Math.Sign(directon.x) * Math.Abs(character.transform.localScale.x), character.transform.localScale.y, character.transform.localScale.z);
        Vector3 newDirection = new Vector3(-directon.y * MathF.Sign(character.transform.localScale.x), MathF.Abs(directon.x), 0);
        character.ObjectReferences.TorsoGuide.position = newDirection + character.Components.HandWithWeapon.transform.position;
    }

    public void WeaponRotateToAim()
    {
        if (character.ActionClimbing.IsClimbing) return;
        //Weapon w = character.ObjectReferences.currentWeapon as Weapon;
        if (character.ObjectReferences.currentWeapon != null)
        {
            character.ObjectReferences.currentWeapon.ItemActions.RotateInHand(character);
        }
    }

    public void Die(bool enabled)
    {
        character.Stats.SetCharacterDead(true);
        //Отключаем Инверсную кинематику
        foreach (Solver2D solver in character.ObjectReferences.CharacterIKManager2D.solvers)
        {
            solver.enabled = !enabled;
        }
        character.ObjectReferences.CharacterIKManager2D.enabled = !enabled;
        //Отключить анимацию
        character.Components.CharacterAnimator.SetEnabled(!enabled);
        //Отключить основной колидер
        character.Components.Capsule2D.enabled = !enabled;
        character.Components.Rb2D.simulated = !enabled;
        //Включить регдолл
        character.Components.CharacterBodyPart.SetEnabled(enabled);
    }

    public void SetViewerInfo(bool enabled)
    {
        character.ObjectReferences.CharacterInfoViewer.SetViewerText(character, enabled);
    }

    public void Move()
    {
        if (character.ActionClimbing.IsClimbing) return;
        velocity += character.Stats.GravityModifier * Physics2D.gravity * Time.fixedDeltaTime;

        IsGrounded = false;
        Vector2 deltaPosition = velocity * Time.fixedDeltaTime;
        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);
        Vector2 move = moveAlongGround * deltaPosition.x;

        ApplyMovement(move, false);
        move = Vector2.up * deltaPosition.y;
        ApplyMovement(move, true);
    }

    private void ApplyMovement(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;
        if (distance > CharacterStats.minMoveDistance) 
        {   
            int count = character.Components.Rb2D.Cast (move, character.Components.CharacterCollisionFilter, hitBuffer, distance + CharacterStats.shellRadius);
            hitBufferList.Clear ();

            for (int i = 0; i < count; i++) 
            {
                if  (hitBuffer[i].collider.gameObject.tag != TagsConst.tagPlatform)
                {
                    hitBufferList.Add (hitBuffer [i]);
                }
                else 
                {
                    MovingPlatform mP = hitBuffer[i].collider.GetComponent<MovingPlatform>();
                    float a = character.Components.Capsule2D.size.x / character.Components.Capsule2D.size.y;
                    float dist = mP.calculateDistance(new Vector3(character.Components.Capsule2D.bounds.center.x, 
                        character.Components.Capsule2D.bounds.center.y - a, 0));
                    if (dist + CharacterStats.shellRadius + mP.DeltaPosition.magnitude > a && (Mathf.Sign(move.x) != Mathf.Sign(mP.SurfaceUp.VectorUp.y) ||
                         Mathf.Sign(move.y) != Mathf.Sign(-mP.SurfaceUp.VectorUp.x)))
                         if (!jumpOff)
                            hitBufferList.Add (hitBuffer [i]);
                }
            }
            jumpOff = false;
            for (int i = 0; i < hitBufferList.Count; i++) 
            {
               Vector2 currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > character.Stats.MinGroundNormalY)
                {
                    IsGrounded = true;
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0)
                    velocity = velocity - projection * currentNormal;

                float modifiedDistance = hitBufferList[i].distance - CharacterStats.shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
                if (distance < 0) move -= hitBufferList[i].normal;
            }
        }
        else if(yMovement) velocity.y = 0;
            else velocity.x = 0;
        Movement += move.normalized * distance;
        //character.Components.Rb2D.MovePosition(Movement);
        character.Components.Rb2D.position = Movement;
        
    }
    public void CharacterAddForce(Vector2 force)
    {
        velocity += force;
    }

    public void ComputeVelocity()
    {
        if (character.ActionClimbing.IsClimbing) return;
        if(isMoveX == 0)
        {
            if (IsGrounded)
                velocity.x = Mathf.Abs(velocity.x) < character.Stats.DampingOnGround * Time.deltaTime ? 0 : velocity.x - character.Stats.DampingOnGround * Mathf.Sign(velocity.x) * Time.deltaTime;
            else
                velocity.x = Mathf.Abs(velocity.x) < character.Stats.DampingOnAir * Time.deltaTime ? 0 : velocity.x - character.Stats.DampingOnAir * Mathf.Sign(velocity.x) * Time.deltaTime;
        }
        else
        {
            if (Mathf.Abs(velocity.x) < character.Stats.MaxSpeed || Mathf.Sign(velocity.x) != isMoveX)
            {
            float acc = IsGrounded ? character.Stats.AccelerationOnGround : character.Stats.AccelerationOnAir;
            velocity.x = velocity.x + acc * isMoveX * Time.deltaTime;
            }
        }
    }
}
