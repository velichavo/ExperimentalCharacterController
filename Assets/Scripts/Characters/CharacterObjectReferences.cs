using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.IK;
using InventoryCharacter;
using WeaponCharacter;
using TMPro;

[System.Serializable]
public class CharacterObjectReferences
{
    Character character;
    private InventoryWithSlots inventory;
    private InventoryWithSlots otherInventory;
    private List<IItem> quickInventory;
    [SerializeField] Transform torsoGuide;
    [SerializeField] IKManager2D iKManager2D;
    [SerializeField] CameraController mainCameraController;
    [SerializeField] CharacterInfoViewer characterInfoViewer;
 

    public IItem currentWeapon {get; set;}
    public IItem LastEquipItem {get; set;}
    public Transform TorsoGuide {get => torsoGuide; set => torsoGuide = value;}
    public InventoryWithSlots CurrentInventory {get => inventory; set => inventory = value;} 
    public IKManager2D CharacterIKManager2D => iKManager2D;
    public CameraController MainCameraController => mainCameraController;
    public CharacterInfoViewer CharacterInfoViewer => characterInfoViewer;
    public InventoryWithSlots OtherInventory {get => otherInventory; set => otherInventory = value;} 

    public void Init(Character newCharacter)
    {
        character = newCharacter;
        inventory = new InventoryWithSlots(newCharacter);
        quickInventory = new List<IItem>(10);
        inventory.AddItemOnStart(character, character.InventoryItemInInspectors);
        characterInfoViewer.gameObject.SetActive(false);
    }
}
