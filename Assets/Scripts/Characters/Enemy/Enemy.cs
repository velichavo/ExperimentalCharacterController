using UnityEngine;

public class Enemy : Character
{
    [SerializeField] Transform followTarget;
    public override void Awake()
    {
        base.Awake();
        followTarget.gameObject.SetActive(false);
    }
    public override void Update() 
    {
        actions.SetFollowPosition(followTarget.position + transform.position);
        base.Update();
    }
}