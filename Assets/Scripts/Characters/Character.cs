using UnityEngine;

public abstract class Character : MonoBehaviour, ICharacter, IInfoViewerble
{
    [SerializeField] protected CharacterStats stats = new CharacterStats();
    [SerializeField] protected CharacterComponents components = new CharacterComponents();
    [SerializeField] protected CharacterObjectReferences objectReferences = new CharacterObjectReferences();
    [SerializeField] protected CharacterActions actions = new CharacterActions();
    [SerializeField] protected CharacterAbility ability = new CharacterAbility();
    [SerializeField] protected CharacterActionThrow actionThrow = new CharacterActionThrow();
    [SerializeField] protected CharacterActionClimbing actionClimbing = new CharacterActionClimbing();
    [SerializeField] InventoryItemInInspector[] inventoryItemInInspectors;

    ItemCollisionSurfaceType surfaceType;
    public CharacterStats Stats => stats;
    public CharacterComponents Components => components;
    public CharacterObjectReferences ObjectReferences => objectReferences;
    public CharacterActions Actions => actions;
    public CharacterAbility Ability => ability;
    public CharacterActionThrow ActionThrow => actionThrow;
    public CharacterActionClimbing ActionClimbing => actionClimbing;
    public InventoryItemInInspector[] InventoryItemInInspectors => inventoryItemInInspectors;
    public ItemCollisionSurfaceType SurfaceType => surfaceType;
    public GameObject Parent{get; private set;}
    
    public virtual void Awake()
    {       
        components.Init(this); 
        actions.Init(this);
        stats.Init(this);
        objectReferences.Init(this);
        ability.Init(this);
        actionThrow.Init(this);
        actionClimbing.Init(this);
        
        AnyStateAnimation[] anyStateAnimations = new AnyStateAnimation[]
        {
            new AnyStateAnimation(RIG.BODY, "none", AnimationVariables.none),
            new AnyStateAnimation(RIG.BODY, "body_idle", AnimationVariables.body_idle, AnimationVariables.body_throw_item_begin, AnimationVariables.body_throw_item_end),
            new AnyStateAnimation(RIG.BODY, "body_walk_forward", AnimationVariables.body_walk_forward),
            new AnyStateAnimation(RIG.BODY, "body_walk_back", AnimationVariables.body_walk_back),
            new AnyStateAnimation(RIG.BODY, "body_jump_forward", AnimationVariables.body_jump_forward),
            new AnyStateAnimation(RIG.BODY, "body_jump", AnimationVariables.body_jump),
            new AnyStateAnimation(RIG.BODY, "body_slow_run_forward", AnimationVariables.body_slow_run_forward),
            new AnyStateAnimation(RIG.BODY, "body_slow_run_back", AnimationVariables.body_slow_run_back),
            new AnyStateAnimation(RIG.BODY, "body_climbing", AnimationVariables.body_climbing),
            new AnyStateAnimation(RIG.BODY, "body_climbing_middle", AnimationVariables.body_climbing_middle),
            new AnyStateAnimation(RIG.BODY, "body_throw_item_begin", AnimationVariables.body_throw_item_begin),
            new AnyStateAnimation(RIG.BODY, "body_throw_item_end", AnimationVariables.body_throw_item_end),

            new AnyStateAnimation(RIG.LEGS, "legs_idle", AnimationVariables.legs_idle),
            new AnyStateAnimation(RIG.LEGS, "legs_walk_forward", AnimationVariables.legs_walk_forward),
            new AnyStateAnimation(RIG.LEGS, "legs_walk_back", AnimationVariables.legs_walk_back),
            new AnyStateAnimation(RIG.LEGS, "legs_jump_forward", AnimationVariables.legs_jump_forward),
            new AnyStateAnimation(RIG.LEGS, "legs_jump", AnimationVariables.legs_jump),
            new AnyStateAnimation(RIG.LEGS, "legs_slow_run_forward", AnimationVariables.legs_slow_run_forward),
            new AnyStateAnimation(RIG.LEGS, "legs_slow_run_back", AnimationVariables.legs_slow_run_back),
            new AnyStateAnimation(RIG.LEGS, "legs_climbing", AnimationVariables.legs_climbing),
            new AnyStateAnimation(RIG.LEGS, "legs_climbing_middle", AnimationVariables.legs_climbing_middle)
        };
        components.CharacterAnimator.AddAnimations(anyStateAnimations);
        components.CharacterBodyPart.Init();
        if (stats.CharacterDead)
        {
            actions.Die(true);
        }
    }
    public virtual void Update() 
    {
        if(stats.CharacterDead) return;

        actions.WeaponRotateToAim();
    }
    public virtual void FixedUpdate()
    {
        components.ToggleDrawSprite();
        actions.Move();
        if(stats.CharacterDead) return;
        actions.ComputeVelocity();
        actionClimbing.TryCheckClimbing();
        //actions.CheckCollisionItem();
        actions.BodyRotateToAim();
    }

    public void ViewInfo(bool enabled)
    {
        actions.SetViewerInfo(enabled);
    }
    public void TakeActionOnItem(Character other)
    {
        if (stats.CharacterDead)
        {
            NewPlayer player = (NewPlayer)other;
            player.Utilities.PlayerUIController.SetOtherInventory(objectReferences.CurrentInventory);
        }
    }
}
