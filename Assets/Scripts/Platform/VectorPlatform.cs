using UnityEngine;

public class VectorPlatform
{
    private float vectorMagnitude;
    private float sqrtVector;
    private Vector2 vectorUp;
    private Vector2 position;
    private Vector2 vectorNormalized;

    /// <summary>
    /// Возвращает единичный вектор направленный налево
    /// </summary>
    public VectorPlatform()
    {
        vectorUp = Vector2.left;
        position = Vector2.zero;
        vectorMagnitude = vectorUp.magnitude;
        vectorNormalized = vectorUp.normalized;
        sqrtVector = calculateSqrtSumPowItem(vectorUp);
    }
    /// <summary>
    /// Возвращает единичный вектор направленный в сторону vector c локальной позицией pos.
    /// Расчитывает длину и нормализованииый вектор только при создании
    /// </summary>
    /// <param name="vector">Вектор с направлением</param>
    /// <param name="pos">Локальная позиция</param>
    public VectorPlatform(Vector2 vector, Vector2 pos)
    {
        vectorUp = vector;
        position = pos;
        vectorMagnitude = vectorUp.magnitude;
        vectorNormalized = vectorUp.normalized;
        sqrtVector = calculateSqrtSumPowItem(vector);
    }

    public float VectorMagnitude
    {
        get{return vectorMagnitude;}
    }
    public Vector2 VectorNormalized
    {
        get{return vectorNormalized;}
    }
    public Vector2 VectorUp
    {
        get{return vectorUp;}
    }
    public Vector2 Position
    {
        get{return position;}
    }
    public float SqrtVector
    {
        get{return sqrtVector;}
    }

    private float calculateSqrtSumPowItem (Vector2 vector)
    {
        return Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y);
    }
}