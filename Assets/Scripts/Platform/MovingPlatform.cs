using UnityEngine;
using UnityEditor;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] Vector3 speed = Vector3.up;
    [SerializeField] Vector3 maxPosition = new Vector2(2, 2);
    [SerializeField] protected ContactFilter2D contactFilter;
    [SerializeField] private int maxCollision = 32;
    private PolygonCollider2D polyCollider;
    private Vector3 currentPosition, startPosition;
    private Vector3 counter;
    private Vector3 deltaPosition;
    private Vector3 nextPosition, prevPositon;
    private VectorPlatform surfaceUp;
    Vector3 origin;
    protected RaycastHit2D[] hitBuffer;

    public Vector3 DeltaPosition
    {
        get{return deltaPosition;}
    }
    public VectorPlatform SurfaceUp 
    {
        get {return surfaceUp;}
    }

    // Start is called before the first frame update
    void Start()
    {
        calculatePlatform();
        calculatePositionRay();
        currentPosition = transform.position;
        startPosition = currentPosition;
        nextPosition = currentPosition;
        prevPositon = currentPosition;
        counter = Vector2.zero;
        contactFilter.useTriggers = false;
        //contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (counter.x > maxPosition.x || counter.x < 0) speed.x *= -1;
        if (counter.y > maxPosition.y || counter.y < 0) speed.y *= -1;
        counter += speed * Time.deltaTime;
        nextPosition = startPosition + counter;
        deltaPosition = nextPosition - transform.position;

        currentPosition = transform.position;
        calculatePositionRay();
        hitBuffer = new RaycastHit2D[maxCollision];
        hitBuffer = Physics2D.RaycastAll(new Vector2(origin.x, origin.y + .2f), surfaceUp.VectorNormalized, surfaceUp.VectorMagnitude, contactFilter.layerMask);
        if (hitBuffer.Length != 0) 
        {
            for (int i = 0; i < hitBuffer.Length; i++)
            {
                if (!hitBuffer[i]) break;
                Character po = hitBuffer[i].collider.GetComponent<Character>();
                if(po != null)
                {
                    float a = po.Components.Capsule2D.size.x / po.Components.Capsule2D.size.y;
                    float dist = calculateDistance(new Vector3(po.Components.Capsule2D.bounds.center.x, po.Components.Capsule2D.bounds.center.y - a, 0));
                    if(dist + CharacterStats.shellRadius > a)
                        hitBuffer[i].rigidbody.position += new Vector2(deltaPosition.x, deltaPosition.y);
                }
            }
        }

        prevPositon = currentPosition;;
        transform.position = nextPosition;
    }

    private void calculatePlatform()
    {
        polyCollider = GetComponent<PolygonCollider2D>();
        Vector2 target = Vector2.left;
        Vector2 pos = Vector2.zero;
        for (int i = 0; i < polyCollider.points.Length; i++)
        {
            Vector2 nextPoint = (i == polyCollider.points.Length - 1) ? polyCollider.points[0] : polyCollider.points[i + 1];
            Vector2 current = nextPoint - polyCollider.points[i];
            current  = new Vector2(current.x * transform.localScale.x, current.y * transform.localScale.y);
            if(current.x < 0  && current.magnitude > target.magnitude)
            {
                target = current;
                pos = polyCollider.points[i];
            }
        }
        surfaceUp = new VectorPlatform(target, pos);
    }
    private void calculatePositionRay()
    {
        origin.x = transform.position.x + surfaceUp.Position.x * transform.localScale.x;
        origin.y = transform.position.y + surfaceUp.Position.y * transform.localScale.y;
    }
    private float calculateDistanceToVector(Vector2 point, Vector2 vector)
    {
        return (vector.y * point.x - vector.x * point.y) / surfaceUp.SqrtVector;
    }
    public float calculateDistance(Vector3 point)
    {
        return calculateDistanceToVector(point - origin, surfaceUp.VectorUp);
    }

    /* private void OnDrawGizmos()
    {
        if (polyCollider == null) return;
        Debug.DrawLine(new Vector3(origin.x, origin.y + .2f, 0), 
            new Vector3(origin.x + surfaceUp.VectorUp.x, origin.y + surfaceUp.VectorUp.y + .2f  , 0), Color.magenta);
        Debug.DrawLine(new Vector3(origin.x + surfaceUp.VectorUp.x / 2, origin.y + surfaceUp.VectorUp.y / 2, 0), 
            new Vector3(origin.x + surfaceUp.VectorUp.x / 2 + surfaceUp.VectorUp.y / 2, origin.y + surfaceUp.VectorUp.y / 2 - surfaceUp.VectorUp.x / 2, 0), Color.magenta);
        Vector3 mP = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Debug.Log(mP);
        if (mP.x > polyCollider.bounds.center.x - polyCollider.bounds.extents.x &&
            mP.x < polyCollider.bounds.center.x + polyCollider.bounds.extents.x &&
            mP.y > polyCollider.bounds.center.y - polyCollider.bounds.extents.y &&
            mP.y < polyCollider.bounds.center.y + polyCollider.bounds.extents.y)
            {

                float dist = calculateDistance(mP);
                Handles.Label(Vector3.zero, dist.ToString());
            }
    } */
}
