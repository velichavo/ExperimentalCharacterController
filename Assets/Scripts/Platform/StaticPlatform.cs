using UnityEngine;

public class StaticPlatform : MonoBehaviour, IEntityCollision
{
    [SerializeField] private ItemCollisionSurfaceType surfaceType;

    public ItemCollisionSurfaceType SurfaceType => surfaceType;

}
