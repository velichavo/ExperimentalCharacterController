using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CameraController : MonoBehaviour
{
    [SerializeField] float cameraSpeed = 1;
    [SerializeField, Range(0, .5f)] float cameraRange = .3f;
    [SerializeField] private float OutOfRange = 1f;
    private static Vector3 worldMousePos;
    private static Vector3 directionToMouse;
    private Vector2 leftBottom;
    private Vector2 rightTop;
    private Camera selfCamera;

    public static Vector3 WorldMousePos
    {
        get {return worldMousePos;}
    }
    public static Vector3 DirectionToMouse { get{return directionToMouse;} }

    private void Start()
    {
        selfCamera = GetComponent<Camera>();
    }
    void Update()
    {
        GetWorldMousePos();
        if (NewPlayer.CharacterInstance == null) return;
        directionToMouse = (worldMousePos - NewPlayer.CharacterInstance.transform.position) * cameraRange;
        transform.position = Vector3.Lerp(transform.position, new Vector3(directionToMouse.x, directionToMouse.y, transform.position.z) + NewPlayer.CharacterInstance.transform.position, cameraSpeed * Time.deltaTime);
        leftBottom = selfCamera.ScreenToWorldPoint(new Vector2 (0, 0)) - Vector3.one * OutOfRange;
        rightTop = selfCamera.ScreenToWorldPoint(new Vector2 (selfCamera.pixelWidth, selfCamera.pixelHeight)) + Vector3.one * OutOfRange;
    }

    private void GetWorldMousePos()
    {
        worldMousePos = selfCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    public Vector3 WorldToScreenPoint(Vector3 worldPosition)
    {  
        return selfCamera.WorldToScreenPoint(worldPosition);
    }
    public bool IsRectCamera(Vector3 position)
    {
        return position.x > leftBottom.x && position.x < rightTop.x && position.y > leftBottom.y && position.y < rightTop.y;
    }
}
