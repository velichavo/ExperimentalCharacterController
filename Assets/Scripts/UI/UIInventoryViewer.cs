using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InventoryCharacter;
using UnityEngine.EventSystems;
using WeaponCharacter;

public class UIInventoryViewer : MonoBehaviour, IDropHandler
{
    [SerializeField] private UIItem UIItemPrefab;
    [SerializeField] private List<UIItem> uIInventoryItems;

    private ScrollRect scrollRect;
    private GridLayoutGroup gridLayoutGroup;
    private UIController uIController;
    private InventoryWithSlots currentInventory;

    public List<UIItem> UIInventoryItems {get => uIInventoryItems; set => uIInventoryItems = value;}
    public ScrollRect ScrollRect {get => scrollRect; set => scrollRect = value;}

    public void Init()
    {
        scrollRect = GetComponentInChildren<ScrollRect>();
        gridLayoutGroup = GetComponentInChildren<GridLayoutGroup>();
        uIController = GetComponentInParent<UIController>();

    }

    public void UIRefreshInventory(object o, InventoryWithSlots inventory)
    {
        if (!uIController.IsShowInventory) return;
        currentInventory = inventory;
        IInventoryItem[] inventoryItems;
        inventoryItems = inventory.GetAllItems();
        foreach (UIItem uIItem in uIInventoryItems)
        {
            uIItem.UIIsEquiped.onValueChanged.RemoveAllListeners();
            uIItem.gameObject.SetActive(false);
        }
        for(int i = 0; i < inventoryItems.Length; i++)
        {
            if(uIInventoryItems.Count < inventoryItems.Length)
            {
                UIItem newUIItem = Instantiate<UIItem>(UIItemPrefab);
                uIInventoryItems.Add(newUIItem);
            }
            uIInventoryItems[i].gameObject.SetActive(true);
            uIInventoryItems[i].transform.SetParent(gridLayoutGroup.transform);
            uIInventoryItems[i].SetInventoryItem(inventoryItems[i]);
            
            if(inventoryItems[i].inventoryItemInfo.isCanEquip)
            {
                uIInventoryItems[i].UIIsEquiped.gameObject.SetActive(true);
                uIInventoryItems[i].UIIsEquiped.isOn = inventoryItems[i].inventoryItemState.isEquipped ? true : false;
                if(inventoryItems[i].Item.ItemActions != null)
                {
                    uIInventoryItems[i].UIIsEquiped.onValueChanged.AddListener(inventoryItems[i].Equip);
                    inventoryItems[i].inventoryItemState.toggle = uIInventoryItems[i].UIIsEquiped;
                }
            }
            else
                uIInventoryItems[i].UIIsEquiped.gameObject.SetActive(false);

            uIInventoryItems[i].UIItemTitle.text = inventoryItems[i].inventoryItemInfo.title;
            uIInventoryItems[i].UIItemCount.text = inventoryItems[i].inventoryItemState.amount.ToString();
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.transform.GetComponent<UIInventoryViewer>()) return;

        eventData.pointerDrag.transform.SetParent(gridLayoutGroup.transform);
        UIItem uIItem = eventData.pointerDrag.transform.GetComponent<UIItem>();
        //Если входящий интвентарь совпадает с теккщим, значит мы перетащили в одном окне и надо просто выйти
        if (uIItem.InventoryItem.Item.ItemComponents.character.ObjectReferences.CurrentInventory == currentInventory) return;

        if (uIItem.InventoryItem.inventoryItemState.toggle)
            uIItem.InventoryItem.inventoryItemState.toggle.isOn = false;
        uIItem.UIIsEquiped.onValueChanged.RemoveAllListeners();
        int amount = uIItem.InventoryItem.Item.ItemComponents.character.ObjectReferences.CurrentInventory.Remove(null, uIItem.InventoryItem, uIItem.InventoryItem.inventoryItemState.amount.Value);
        NewPlayer player = null;
        switch(uIItem.InventoryItem.Item.ItemComponents.character)
        {
            case Enemy:
                player = currentInventory.character as NewPlayer;
                player.Utilities.PlayerUIController.PlayerUIInventory.OtherUIInventoryViewer.uIInventoryItems.Remove(uIItem);
                Debug.Log("Enemy");
                break;
            case NewPlayer:
                player = uIItem.InventoryItem.Item.ItemComponents.character as NewPlayer;
                player.Utilities.PlayerUIController.PlayerUIInventory.PlayerUIInventoryViewer.uIInventoryItems.Remove(uIItem);
                break;
        }
        currentInventory.TryToAdd(currentInventory.character, uIItem.InventoryItem, amount);

        Destroy(uIItem.gameObject);
    }
}
