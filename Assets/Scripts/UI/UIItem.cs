using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class UIItem : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private Canvas mainCanvas;
    private RectTransform itemRect;
    [SerializeField] private Toggle toggle;
    [SerializeField] private GridLayoutGroup layoutGroup;
    [SerializeField] private TextMeshProUGUI itemTitle;
    [SerializeField] private TextMeshProUGUI itemCount;

    public TextMeshProUGUI UIItemTitle {get => itemTitle;}
    public TextMeshProUGUI UIItemCount {get => itemCount;}
    public Toggle UIIsEquiped {get => toggle;}
    public CanvasGroup BlockRaycast {get; set;}
    public IInventoryItem InventoryItem {get; private set;}

    private void Start()
    {
        InitItem();
    }

    public void InitItem()
    {
        mainCanvas = GetComponentInParent<Canvas>();
        itemRect = GetComponent<RectTransform>();
        layoutGroup = GetComponentInParent<GridLayoutGroup>();
        BlockRaycast = GetComponent<CanvasGroup>();
        itemRect.position = new Vector3(itemRect.position.x, itemRect.position.y, 0);
        itemRect.localScale = Vector3.one;
    }
    public void SetInventoryItem(IInventoryItem inventoryItem)
    {
        this.InventoryItem = inventoryItem;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetParent(mainCanvas.transform);
        BlockRaycast.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        itemRect.anchoredPosition += eventData.delta / mainCanvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.parent = layoutGroup.transform;
        transform.position = Vector3.zero;
        BlockRaycast.blocksRaycasts = true;
    }

    public UIItem Clone()
    {
        return Instantiate<UIItem>(this);
    }
}
