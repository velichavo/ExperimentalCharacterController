using UnityEngine;
using TMPro;
using WeaponCharacter;
using InventoryCharacter;

public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI uIHealthPoint;
    [SerializeField] TextMeshProUGUI uIAmmoClip;
    [SerializeField] TextMeshProUGUI uIAmmoInventory;
    private Character character;
    public UIInventory PlayerUIInventory {get; private set;}
    public bool IsShowInventory {get; private set;}
    public void Init(Character character)
    {
        this.character = character;
        PlayerUIInventory = GetComponentInChildren<UIInventory>();
        PlayerUIInventory.PlayerUIInventoryViewer.Init();
        PlayerUIInventory.OtherUIInventoryViewer.Init();
        character.ObjectReferences.CurrentInventory.OnInventoryItemRefreshEvent += PlayerUIInventory.PlayerUIInventoryViewer.UIRefreshInventory;
        IsShowInventory = false;
        PlayerUIInventory.gameObject.SetActive(false);
    }

    public void ToggleInventory()
    {
        if (IsShowInventory) 
        {
            IsShowInventory = false;
            PlayerUIInventory.gameObject.SetActive(false);
            if(character.ObjectReferences.OtherInventory != null)
            {
                character.ObjectReferences.OtherInventory.OnInventoryItemRefreshEvent -= PlayerUIInventory.OtherUIInventoryViewer.UIRefreshInventory;
                character.ObjectReferences.OtherInventory = null;
            }

        }
        else
        {
            IsShowInventory = true;
            PlayerUIInventory.gameObject.SetActive(true);
            PlayerUIInventory.PlayerUIInventoryViewer.UIRefreshInventory(character, character.ObjectReferences.CurrentInventory);
            if(character.ObjectReferences.OtherInventory != null)
            {
                PlayerUIInventory.OtherUIInventoryViewer.gameObject.SetActive(true);
                PlayerUIInventory.OtherUIInventoryViewer.UIRefreshInventory(character, character.ObjectReferences.OtherInventory);
            }
            else
                PlayerUIInventory.OtherUIInventoryViewer.gameObject.SetActive(false);
        }
    }

    public void SetOtherInventory(InventoryWithSlots otherInventory)
    {
        otherInventory.OnInventoryItemRefreshEvent += PlayerUIInventory.OtherUIInventoryViewer.UIRefreshInventory;
        character.ObjectReferences.OtherInventory = otherInventory;
        ToggleInventory();
    }

    public void SetTextHealthPoint(int hp)
    {
        uIHealthPoint.text = hp.ToString();
    }

    public void SetTextClip(int value)
    {
        if (value == -1)
            uIAmmoClip.text = "-";
        else
            uIAmmoClip.text = value.ToString();
    }

    public void SetTextAmmo(int value)
    {
        if (value == -1)
            uIAmmoInventory.text = "-";
        else
            uIAmmoInventory.text = value.ToString();
    }

    /*public void SetTextAmmo(IItem item) // Разделить на 2 метода для обновления ClipText и InventoryText
    {
        switch (item)
        {
            case Weapon weapon:
                uIAmmoClip.text = weapon ? weapon.Stats.CurrentClipCapacity.ToString() : "-";
                InventoryItemInfo info = ((WeaponInfoSO)weapon.ItemComponents.InventoryItemInfo).AmmoType;
                uIAmmoInventory.text = info ? character.ObjectReferences.CurrentInventory.GetItemAmount(info).ToString() : "-";
                break;
            case Ammo ammo:
                if (ammo.ItemComponents.inventoryItem.inventoryItemState.amount.Value >= ammo.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot)
                {
                    uIAmmoClip.text = ammo.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot.ToString();
                    uIAmmoInventory.text = (ammo.ItemComponents.inventoryItem.inventoryItemState.amount.Value - ammo.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot).ToString();
                }
                else
                {
                    uIAmmoClip.text = ammo.ItemComponents.inventoryItem.inventoryItemState.amount.ToString();
                    uIAmmoInventory.text = "0";
                }
                break;
            default:
                uIAmmoClip.text = "-";
                uIAmmoInventory.text = "-";
                break;
        }
    }*/
    private void OnDestroy()
    {
        character.ObjectReferences.CurrentInventory.OnInventoryItemRefreshEvent -= PlayerUIInventory.PlayerUIInventoryViewer.UIRefreshInventory;
    }
}
