using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour
{
    [SerializeField] private UIInventoryViewer playerUIInventoryViewer;
    [SerializeField] private UIInventoryViewer otherInventoryViewer;

    public UIInventoryViewer PlayerUIInventoryViewer=> playerUIInventoryViewer;
    public UIInventoryViewer OtherUIInventoryViewer => otherInventoryViewer;
}
