using System;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public event Action OnThrowing;
    public const float MaxSpeed = .1f;
    [SerializeField] ContactFilter2D layerCollision;

    public ContactFilter2D LayerCollision => layerCollision;

    private void FixedUpdate()
    {
        OnThrowing?.Invoke();
    }
}
