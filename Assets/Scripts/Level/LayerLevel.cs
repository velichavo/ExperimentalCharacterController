using UnityEngine;

public class LayerLevel : MonoBehaviour
{
    [SerializeField] private Transform layerItem;

    public Transform LayerItem => layerItem;
}
