using UnityEngine;
using InventoryCharacter;

namespace WeaponCharacter
{
    [CreateAssetMenu(fileName = "InventoryItemInfo", menuName = "Gameplay/Items/Create New Ammo")]
    public class AmmoInfoSO : InventoryItemInfo, IAmmoInfo
    {
        [SerializeField] private float damageMultiplier;
        [SerializeField] private string ammoType;
        [SerializeField] private Sprite spriteAmmo;
        [SerializeField] private Vector2 offsetCollider;

        public float DamageMultiplier => damageMultiplier;
        public string AmmoTypeText => ammoType;
        public Sprite SpriteAmmo => spriteAmmo;
        public Vector2 OffsetCollider => offsetCollider;
    }
}