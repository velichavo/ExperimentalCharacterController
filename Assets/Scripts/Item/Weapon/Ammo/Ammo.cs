using UnityEngine;

namespace WeaponCharacter
{
    public class Ammo : BaseItem
    {
        [SerializeField] protected AmmoComponents ammoComponents = new AmmoComponents();
        [SerializeField] protected AmmoActions ammoActions = new AmmoActions();
        [SerializeField] protected AmmoStats ammoStats = new AmmoStats();
        [SerializeField] protected ItemMovement ammoMovement = new ItemMovement();
        protected ItemImpactPlaySound ammoImpactPlaySound = new ItemImpactPlaySound();

        public override void Init()
        {
            SetModules();
            base.Init();
        }

        public override BaseItem Clone()
        {
            Ammo ammo = Instantiate<Ammo>(this);
            ammo.SetModules();
            return ammo;
        }

        public override void ViewInfo(bool enabled)
        {
            
        }
        private void SetModules()
        {
            components = ammoComponents;
            actions = ammoActions;
            stats = ammoStats;
            movement = ammoMovement;
            itemImpactPlaySound = ammoImpactPlaySound;
        }
    }
}