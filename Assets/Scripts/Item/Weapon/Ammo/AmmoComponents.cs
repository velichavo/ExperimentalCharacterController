using UnityEngine;

namespace WeaponCharacter
{
    [System.Serializable]
    public class AmmoComponents : ItemComponents
    {
        [SerializeField] private AmmoInfoSO sourceConfig;

        public override void Init(IItem newItem)
        {
            if (inventoryItemInfo == null)
                inventoryItemInfo = sourceConfig;
            base.Init(newItem);
            baseCollider = item.ItemGameObject.GetComponent<PolygonCollider2D>();
            SetItemInfo();
        }

        public override void SetItemInfo()
        {
            if(inventoryItemInfo != null)
            {
                base.SetItemInfo();
                SetCollider();
                if (item.ItemStats.CurrentClipCapacity.Value <= 0)
                    item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = inventoryItemInfo.maxItemsInInventorySlot;
                else
                    item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = item.ItemStats.CurrentClipCapacity.Value;
            }
        }
    }
}
