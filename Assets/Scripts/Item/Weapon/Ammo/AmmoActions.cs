using UnityEngine;

public class AmmoActions : IItemActions
{
    private BaseItem item;
    public virtual void Init(BaseItem newItem)
    {
        item = newItem;
    }
    public virtual BaseItem Equip(Character character, bool isEquiped, bool isThrowing = false)
    {
        if(item.ItemComponents.InventoryItemInfo.isCanEquip)
        {
            if(isEquiped)
            {
                // обновление UI
                if(item.ItemComponents.inventoryItem.inventoryItemState.amount.Value > item.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot)
                    item.ItemStats.CurrentClipCapacity.Value = item.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot;
                else
                    item.ItemStats.CurrentClipCapacity.Value = item.ItemComponents.inventoryItem.inventoryItemState.amount.Value;
                item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = item.ItemComponents.inventoryItem.inventoryItemState.amount.Value;

                BaseItem newItem = item.Clone();
                newItem.ItemComponents.InventoryItemInfo = item.ItemComponents.InventoryItemInfo;
                newItem.Init();
                newItem.ItemComponents.character = character;
                newItem.ItemComponents.inventoryItem.inventoryItemState.toggle = item.ItemComponents.inventoryItem.inventoryItemState.toggle;
                newItem.ItemGameObject.SetActive(true);
                newItem.ItemGameObject.transform.SetParent(character.Components.GripSolver.GetChain(0).effector.transform);
                newItem.ItemGameObject.transform.localPosition = Vector3.zero;
                newItem.ItemGameObject.transform.localRotation = Quaternion.identity;
                newItem.ItemComponents.Rb2D.simulated = false;
                return newItem;
            }
            else
            {
                if(character.ObjectReferences.currentWeapon != null)
                {
                    character.ObjectReferences.currentWeapon.Destroy();
                    character.ObjectReferences.currentWeapon = null;
                }
                return null;
            }
        }
        return null;
    }

    public void Attack(Character character, GetKeyState state)
    {
        character.ActionThrow.Swing(state);
    }

    public void RotateInHand(Character character)
    {
        //item.transform.position = Vector3.zero;
        //item.transform.rotation = Quaternion.identity;
    }
    public virtual void TryToThrowItem(Character character)
    {
        ThrowItem(character);
        SubscribeCollision();
    }

    protected virtual void SubscribeCollision()
    {
        item.ItemComponents.Manager.OnThrowing += item.ItemMovement.UpdateMovement;
        item.StartCoroutine(item.ItemMovement.EndCollision());
    }

    protected virtual void ThrowItem(Character character)
    {
        character.ObjectReferences.currentWeapon = null;
        IInventoryItem equipInventoryItem = character.ObjectReferences.CurrentInventory.GetItem(item.ItemComponents.InventoryItemInfo);
        equipInventoryItem.Equip(false);
        int remainder = character.ObjectReferences.CurrentInventory.Remove(character, equipInventoryItem, item.ItemComponents.inventoryItem.inventoryItemState.amount.Value);
        item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = remainder;
        item.ItemGameObject.transform.SetParent(item.ItemComponents.Manager.transform);
        

        item.ItemComponents.Rb2D.simulated = true;
        item.ItemComponents.Rb2D.AddForce(character.Actions.followTarget - character.Components.HandWithWeapon.position, ForceMode2D.Impulse);
    }

    public void SetItemEnabled(bool enabled)
    {
        item.ItemComponents.Rb2D.simulated = enabled;
        item.ItemGameObject.SetActive(enabled);
    }
}

