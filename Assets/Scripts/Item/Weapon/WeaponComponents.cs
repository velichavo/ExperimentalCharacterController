using InventoryCharacter;
using UnityEngine;

namespace WeaponCharacter
{
    [System.Serializable]
    public class WeaponComponents : ItemComponents
    {
        private Transform weaponGrip;
        private Transform weaponHandGuard;
        [SerializeField] private WeaponInfoSO sourceConfig;

        public Transform WeaponGrip => weaponGrip;
        public Transform WeaponHandGuard => weaponHandGuard;

        public override void Init(IItem newItem)
        {
            if (sourceConfig)
                inventoryItemInfo = sourceConfig;
            base.Init(newItem);
            baseCollider = item.ItemGameObject.GetComponent<PolygonCollider2D>();
            weaponGrip = newItem.ItemGameObject.GetComponentInChildren<EffectorGrip>().transform;
            weaponHandGuard = newItem.ItemGameObject.GetComponentInChildren<EffectorHandguard>().transform;
            SetItemInfo();
        }

        public override void SetItemInfo()
        {
            if (inventoryItemInfo != null && inventoryItem == null)  
            {
                base.SetItemInfo();
                SetCollider();
                spriteRenderer.transform.localPosition = sourceConfig.SpritePostion;
                weaponGrip.localPosition = sourceConfig.GripPositon;
                weaponHandGuard.localPosition = sourceConfig.HandguardPosition;
                weaponGrip.gameObject.SetActive(false);
                weaponHandGuard.gameObject.SetActive(false);
                //Устанавливаем размер обоймы. Если меньше 0 или больше объема, то ставим максимальный
                if (item.ItemStats.CurrentClipCapacity.Value > sourceConfig.ClipCapacity || item.ItemStats.CurrentClipCapacity.Value < 0)
                    item.ItemStats.CurrentClipCapacity.Value = sourceConfig.ClipCapacity;
            }
        }
    }
}
