using UnityEngine;

namespace WeaponCharacter
{
    public class Weapon : BaseItem
    {
        [SerializeField] private WeaponActions weaponActions = new WeaponActions();
        [SerializeField] private WeaponComponents weaponComponents = new WeaponComponents();
        [SerializeField] private WeaponStats weaponStats = new WeaponStats();
        [SerializeField] private ItemMovement weaponMovement = new ItemMovement();
        protected ItemImpactPlaySound weaponImpactPlaySound = new ItemImpactPlaySound();

        public override void Init()
        {
            //component создается первым, потому что там конфиг
            SetModules();
            base.Init();
        }
        public override BaseItem Clone()
        {
            Weapon weapon = Instantiate<Weapon>(this);
            weapon.SetModules();
            return weapon;
        }
        private void SetModules()
        {
            components = weaponComponents;
            actions = weaponActions;
            stats = weaponStats;
            movement = weaponMovement;
            itemImpactPlaySound = weaponImpactPlaySound;
        }

        void OnDrawGizmos()
        {
            /*Handles.RadiusHandle(Quaternion.identity, transform.position, .03f, false);
            if (components.WeaponInventoryItem == null) return;
            Weapon w = (Weapon)components.WeaponInventoryItem.Item;
            if (w)
                Handles.Label(w.transform.position + Vector3.up, w.ToString());*/
        }
    }
}