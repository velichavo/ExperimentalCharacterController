using UnityEngine;
using UnityEngine.Rendering.Universal;
using System.Collections;

public abstract class BaseEffect : MonoBehaviour
{
    public const float EnableLightTimeout = .1f;
    [SerializeField] private Light2D ligthEffect;

    public virtual void OnEnable()
    {
        StartCoroutine(ShowLightTimeout());
    }

    private IEnumerator ShowLightTimeout()
    {
        yield return new WaitForSeconds(EnableLightTimeout);
        if(ligthEffect)
            ligthEffect.enabled = false;
    }
    public void OnParticleSystemStopped()
    {
        Destroy(gameObject);
    }
}