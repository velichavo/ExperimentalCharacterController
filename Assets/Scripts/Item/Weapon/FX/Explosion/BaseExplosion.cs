using UnityEngine;

public class BaseExplosion : BaseEffect
{
    [SerializeField] private ContactFilter2D filter;
    private RaycastHit2D[] hit;

    public float damage {get; set;}
    public float damageRadius {get; set;}

    public override void OnEnable()
    {
        filter.useTriggers = false;
        filter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        filter.useLayerMask = true;
        base.OnEnable();
    }

    public void Start()
    {
        ApplyDamage();
    }
    private void ApplyDamage()
    {
        hit = Physics2D.CircleCastAll(transform.position, damageRadius, Vector2.zero, 0, filter.layerMask);
        Debug.Log($"mask: {filter.layerMask.value}, lenght: {hit.Length}, damageRadius: {damageRadius}, gameObject.layer: {gameObject.layer}");
        for(var i = 0; i < hit.Length; i++)
            if (hit[i].collider != null)
            {
                IEntityCollisionAction bc = hit[i].transform.GetComponent<IEntityCollisionAction>();
                if (bc != null)
                {
                    Debug.Log(bc);
                    bc.OnItemExplosion(this);
                    bc.ApplyForceEntity(this);
                }
            }
    }
}