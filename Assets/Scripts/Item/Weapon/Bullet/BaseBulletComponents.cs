using UnityEngine;
using UnityEngine.Rendering.Universal;

[System.Serializable]
public class BaseBulletComponents
{
    [SerializeField] private ContactFilter2D filter;
    private BaseBullet bullet;
    public ContactFilter2D contactFilter => filter;
    public bool destroy {get; set;}
    public SpriteRenderer spriteRenderer {get; private set;}
    public Light2D light {get; set;}
    public ParticleSystem particleDust {get; private set;}
    public AudioSource BulletAudioSource {get; private set;}

    public BaseBulletComponents(BaseBullet newBullet)
    {
        bullet = newBullet;
        destroy = false;
        spriteRenderer = bullet.GetComponent<SpriteRenderer>();
        filter.useTriggers = false;
        filter.SetLayerMask(Physics2D.GetLayerCollisionMask(bullet.gameObject.layer));
        filter.useLayerMask = true;
        particleDust = bullet.GetComponent<ParticleSystem>();
        light = bullet.GetComponent<Light2D>();
        BulletAudioSource = bullet.GetComponent<AudioSource>();
        //light.lightCookieSprite = bullet.bulletInfo.bulletSpriteLight;
    }
}