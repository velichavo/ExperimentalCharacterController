using System;
using UnityEngine;

public abstract class BaseBullet : MonoBehaviour
{
    [SerializeField] public Transform debugObject;
    [SerializeField] public float scaleRatioAlive;
    [SerializeField] public float scaleRatioDie;
    [SerializeField] protected BaseBulletComponents components;
    //protected SpriteRenderer spriteRenderer;
    [SerializeField] protected BulletInfo info;
    protected BaseBulletAction action;
    protected BulletImpactPlayEffect itemImpactPlaySound = new BulletImpactPlayEffect();

    public GameObject parent {get; private set;}
    public float speed { get; set; }
    public float damage { get; set; }
    public Vector3 direction { get; set; }
    public Type type { get; set; }
    public BaseBulletAction bulletAction => action; 
    public BulletInfo bulletInfo => info;
    public BaseBulletComponents bulletComponents => components;
    public BulletImpactPlayEffect ItemImpactPlaySound => itemImpactPlaySound;

    public virtual void Init(GameObject sender, Transform transform, Vector3 scale, float speed, float damage, float accuracy)
    {
        parent = sender;
        action = new BaseBulletAction(this);
        components = new BaseBulletComponents(this);
        itemImpactPlaySound.Init(this);
        this.transform.position = transform.position;
        this.direction = (scale.x * transform.right).normalized;
        this.direction += new Vector3(-direction.y * accuracy, direction.x * accuracy, 0);
        this.transform.rotation = Quaternion.FromToRotation(Vector3.right, direction);
        this.speed = speed;
        this.damage = damage;
        components.spriteRenderer.sprite = info.bulletSprite;
        components.spriteRenderer.transform.localScale = new Vector3(0.01f, .5f, 1);
    }

    private void FixedUpdate()
    {
        action.Move(); 
        action.ComputeScale(); 
               
    }

    /*private void OnDrawGizmos()
    {
        //if (bulletComponents.destroy)
            Gizmos.DrawLine(transform.position, transform.position + direction * 10);
    }*/
}
