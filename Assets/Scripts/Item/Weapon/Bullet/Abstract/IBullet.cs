using UnityEngine;
using System;

public interface IBullet
{
    GameObject parent {get;}
    float speed {get; set;}
    float damage {get; set;}
    Vector3 direction {get; set;}
    Type type {get;}
    BaseBulletAction bulletAction {get;}
    BulletInfo bulletInfo {get;}

    public void Init(GameObject sender, Vector3 position, Vector3 direction, float speed, float damage);
}