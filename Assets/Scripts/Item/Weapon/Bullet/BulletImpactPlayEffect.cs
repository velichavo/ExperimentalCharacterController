using UnityEngine;

public class BulletImpactPlayEffect
{
    BaseBullet bullet;

    public void Init(BaseBullet newBullet)
    {
        bullet = newBullet;
    }
    public void PlayEffect(IEntityCollision collision, Vector2 normal)
    {
        GameObject.Instantiate<BaseBulletImpact>(bullet.bulletInfo.BaseEffectObject[(int)collision.SurfaceType], bullet.transform.position, Quaternion.FromToRotation(Vector3.right, normal));
        bullet.bulletComponents.BulletAudioSource.PlayOneShot(bullet.bulletInfo.SoundImpact[(int)collision.SurfaceType]);
    }
}