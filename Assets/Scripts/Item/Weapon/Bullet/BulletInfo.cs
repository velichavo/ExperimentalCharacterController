using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletInfo", menuName = "Gameplay/Bullet")]
public class BulletInfo : ScriptableObject
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private Sprite spriteLight;
    [SerializeField] private float timeBeforeDestroy;
    [SerializeField] private BaseBulletImpact[] baseEffect = new BaseBulletImpact[10];
    [SerializeField] private AudioClip[] soundImpact = new AudioClip[10];
    public Sprite bulletSprite => sprite;
    public Sprite bulletSpriteLight => spriteLight;
    public float timeDestroy => timeBeforeDestroy;
    public BaseBulletImpact[] BaseEffectObject => baseEffect;
    public AudioClip[] SoundImpact => soundImpact;
}
