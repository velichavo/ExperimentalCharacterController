using System;
using UnityEngine;

public class BaseBulletAction
{
    private Vector3 currentScale = new Vector3(.01f, 1, 1);
    private Vector3 movement;
    private BaseBullet bullet;
    private RaycastHit2D[] hit2D;
    public BaseBulletAction(BaseBullet newBullet)
    {
        bullet = newBullet;
    }
    public void ComputeScale()
    {
        if (bullet.bulletComponents.destroy)
        {
            if (currentScale.x > .01f)
            {
                currentScale.x -= currentScale.x / bullet.scaleRatioDie;
            }
            else
            {
                GameObject.Destroy(bullet.gameObject);
            }
        }
        else 
        {
            if (currentScale.x < bullet.speed * bullet.scaleRatioAlive)
                currentScale.x += bullet.speed * Time.deltaTime * 2;
        }
        currentScale.Set(currentScale.x, bullet.transform.localScale.y, bullet.transform.localScale.z);
        bullet.bulletComponents.spriteRenderer.transform.localScale = currentScale;
    }
    public void Move()
    {
        if(bullet.bulletComponents.destroy) return;
        ComputeCollision();
    }

    private void ComputeCollision()
    {
        movement = bullet.direction * bullet.speed * Time.deltaTime;
        hit2D = Physics2D.RaycastAll(bullet.transform.position, bullet.direction, movement.magnitude, bullet.bulletComponents.contactFilter.layerMask);
        for(var i = 0; i < hit2D.Length; i++)
        {
            if(hit2D[i])
            {
                IEntityCollision collision = hit2D[i].transform.GetComponent<IEntityCollision>();
                IEntityCollisionAction collisionAction = collision as IEntityCollisionAction;
                if(collisionAction != null)
                {
                    if(!object.ReferenceEquals(bullet.parent.gameObject, collisionAction.CharacterParent))
                        collisionAction.OnBulletCollision(bullet);
                    else
                    {
                        continue;
                    }
                }
                if(collision != null)
                {
                    bullet.bulletComponents.destroy = true;
                    bullet.transform.position += bullet.direction * hit2D[i].distance;
                    currentScale.x += hit2D[i].distance * 2;
                    bullet.ItemImpactPlaySound.PlayEffect(collision, hit2D[i].normal);
                    return;
                }
            }
        }
        bullet.transform.position += movement;
    }
}
