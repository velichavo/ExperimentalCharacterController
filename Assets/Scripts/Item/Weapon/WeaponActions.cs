using System;
using System.Collections;
using UnityEngine;
using UnityEngine.U2D.IK;

namespace WeaponCharacter
{
    public enum ShootState {None = 0, Ready, StartShoots, Shoots, TimeoutShoot, EndShoots, Reload, NoAmmo}
    public class WeaponActions : IItemActions
    {
        //public event Action<IItem> OnChangeItemAmount;
        private Weapon weapon;
        private WeaponComponents components;
        private WeaponInfoSO info;
        private ShootState shootState;
        private GetKeyState keyState;
        private System.Random random;
        public void Init(BaseItem newWeapon)
        {
            weapon = (Weapon)newWeapon;
            components = (WeaponComponents)(weapon.ItemComponents);
            info = (WeaponInfoSO)(weapon.ItemComponents.InventoryItemInfo);
            shootState = ShootState.Ready;
            random = new System.Random();
        }

        public BaseItem Equip(Character character, bool isEquiped, bool isThrowing = false)
        {
            weapon.gameObject.SetActive(isEquiped);
            shootState = ShootState.Ready;
            weapon.ItemComponents.parent = isEquiped ? character.gameObject : null;
            character.Components.CurrentChainRightHand = isEquiped ? components.WeaponGrip : null;
            character.Components.ChainRightHand.target  = character.Components.CurrentChainRightHand;
            character.Components.CurrentChainLeftHand = isEquiped ? components.WeaponHandGuard : null;
            character.Components.ChainLeftHand.target = character.Components.CurrentChainLeftHand;
            return isEquiped ? weapon : null;
        }

        public void RotateInHand(Character character)
        {

            Vector3 newPosition = character.Components.HandWithWeapon.position;
            Vector3 directon = (character.Actions.followTarget - newPosition) * Vector2.one;
            Vector3 normalDirection = directon.normalized;
            Vector3 shiftWeapon = Vector3.zero;

            if (normalDirection.y < 0)
                shiftWeapon = new Vector3(-((WeaponInfoSO)components.InventoryItemInfo).WeaponShiftCoefficientWhenTurning * normalDirection.y * character.transform.localScale.x,
                    ((WeaponInfoSO)components.InventoryItemInfo).WeaponShiftCoefficientWhenTurning * normalDirection.y / 2, 0);
                    
            Vector3 newDirection = new Vector3(-directon.y * MathF.Sign(character.transform.localScale.x), MathF.Abs(directon.x), 0);
            weapon.transform.position =  character.Components.HandWithWeapon.transform.position + shiftWeapon;
            weapon.transform.rotation = Quaternion.LookRotation(Vector3.forward, newDirection);
            weapon.transform.localScale = character.transform.localScale;
            newPosition = components.ItemSpriteRenderer.transform.position;
            directon = (character.Actions.followTarget - newPosition) * Vector2.one;
            if(Math.Abs(directon.x) > 1 || Math.Abs(directon.y) > 1)
            {
                newDirection = new Vector3(-directon.y * MathF.Sign(weapon.transform.localScale.x), directon.x * MathF.Sign(weapon.transform.localScale.x), 0);
            }
            weapon.ItemComponents.ItemSpriteRenderer.transform.rotation = Quaternion.LookRotation(Vector3.forward, newDirection);
        }

        public void TryToThrowItem(Character character)
        {
            weapon.ItemComponents.inventoryItem.Equip(false);
            character.ObjectReferences.CurrentInventory.Remove(character, weapon.ItemComponents.inventoryItem);
            SetItemEnabled(true);
            weapon.ItemComponents.Rb2D.AddForce(character.Actions.followTarget - character.Components.HandWithWeapon.position, ForceMode2D.Impulse);
        }

        public void SetItemEnabled(bool enabled)
        {
            weapon.ItemComponents.Rb2D.simulated = enabled;
            weapon.gameObject.SetActive(enabled);
        }
        public void Attack(Character character, GetKeyState state)
        {
            keyState = state;
            switch (shootState)
            {
                case ShootState.Ready:
                    if(weapon.ItemStats.CurrentClipCapacity.Value == 0) ReloadWeapon();
                    else
                    {
                        if(info.StartShootTime > 0)
                            weapon.StartCoroutine(StartShoot());
                        else 
                            shootState = ShootState.Shoots;
                    }
                    break;
                case ShootState.Shoots:
                    for(var i = 0; i < info.BulletAmountPerShot; i++)
                    {
                        BaseBullet newBullet = GameObject.Instantiate<BaseBullet>(info.bulletInstance);
                        newBullet.Init(character.gameObject, components.ItemSpriteRenderer.transform, weapon.transform.localScale, info.BulletSpeed, info.Damage, (1 - ((float)random.Next() / int.MaxValue) * 2) * (100 - info.Accuracy) / 1000);
                    }
                    weapon.StartCoroutine(TimeoutShoot());
                    EndShoot();
                    break;
                case ShootState.TimeoutShoot:
                    EndShoot();
                    break;
                case ShootState.NoAmmo:
                    if (state == GetKeyState.GetKeyDown)
                    {
                        NoAmmo();
                    }
                    break;
                default:
                    break;
            }
        }

        public void ReloadWeapon()
        {
            if(weapon.ItemStats.CurrentClipCapacity.Value == info.ClipCapacity) return;
            if(shootState != ShootState.Reload)
                weapon.StartCoroutine(Reload());
        }

        public void UnloadWeapon()
        {
            IInventoryItem ammoInventoryItem = weapon.ItemComponents.character.ObjectReferences.CurrentInventory.GetItem(info.AmmoType);
            if(ammoInventoryItem == null)
            {
                ammoInventoryItem = components.character.ObjectReferences.CurrentInventory.CreateItem(components.character, ammoInventoryItem.inventoryItemInfo).ItemComponents.inventoryItem;
            }
            TryUploadWeapon(ammoInventoryItem);
        }
        private void TryUploadWeapon(IInventoryItem inventoryItem)
        {
            components.character.ObjectReferences.CurrentInventory.TryToAdd(components.character, inventoryItem, weapon.ItemStats.CurrentClipCapacity.Value);
            weapon.ItemStats.CurrentClipCapacity.Value = 0;
        }

        private IEnumerator TimeoutShoot()
        {
            weapon.ItemStats.CurrentClipCapacity.Value -= 1;
            //OnChangeItemAmount?.Invoke(weapon);
            shootState = ShootState.TimeoutShoot;
            components.ItemAudioSource.PlayOneShot(info.ShootSound);
            yield return new WaitForSeconds(info.ShootTime);
            if (keyState == GetKeyState.GetKeyUp)
                shootState = ShootState.Ready;
            else
                shootState = ShootState.Shoots;
            if(weapon.ItemStats.CurrentClipCapacity.Value == 0)
            {
                EndShootSoundPlay();
                weapon.StartCoroutine(Reload());
            }
        }
        private IEnumerator Reload()
        {
            //Ищу патроны к оружию в инвентаре
            IInventoryItem ammoInventoryItem = weapon.ItemComponents.character.ObjectReferences.CurrentInventory.GetItem(info.AmmoType);
            if (ammoInventoryItem != null)
            {
                if(!info.IsPlayReloadSoundEqualClipCapacity)
                    TryUploadWeapon(ammoInventoryItem);
                //OnChangeItemAmount?.Invoke(weapon);
                int cc = info.ClipCapacity < ammoInventoryItem.inventoryItemState.amount.Value ? info.ClipCapacity : ammoInventoryItem.inventoryItemState.amount.Value;
                int ammoLeft = cc - weapon.ItemStats.CurrentClipCapacity.Value;
                Debug.Log(ammoLeft);
                float rt = info.ReloadTime;
                int cp  = 1;
                if (info.IsPlayReloadSoundEqualClipCapacity)
                {
                    rt = info.ReloadTime / info.ClipCapacity * ammoLeft;
                    cp = ammoLeft ;
                }
                shootState = ShootState.Reload;
                for(int i = 0; i < cp; i++)
                {
                    components.ItemAudioSource.PlayOneShot(info.ReloadSound);
                    yield return new WaitForSeconds(rt / cp);
                    weapon.ItemStats.CurrentClipCapacity.Value += weapon.ItemComponents.character.ObjectReferences.CurrentInventory.Remove(weapon.ItemComponents.character, ammoInventoryItem, ammoLeft / cp);
                    //OnChangeItemAmount?.Invoke(weapon);
                }
            shootState = ShootState.Ready;
            }
            else
                NoAmmo();
        }
        private IEnumerator StartShoot()
        {
            shootState = ShootState.StartShoots;
            components.ItemAudioSource.clip = info.StartShootSound;
            components.ItemAudioSource.Play();
            yield return new WaitForSeconds(info.StartShootTime);
            if (keyState == GetKeyState.GetKeyUp)
                shootState = ShootState.Ready;
            else
                shootState = ShootState.Shoots;
        }
        private void EndShoot()
        {
            if (keyState == GetKeyState.GetKeyUp)
            {
                if (info.EndShootTime > 0) 
                    EndShootSoundPlay();
            }
        }
        private void EndShootSoundPlay()
        {
            if (info.EndShootSound)
            {
                components.ItemAudioSource.clip = info.EndShootSound;
                components.ItemAudioSource.Play();
            }
        }

        private void NoAmmo()
        {
            components.ItemAudioSource.PlayOneShot(info.NoAmmoSound);
            shootState = ShootState.NoAmmo;
        }
    }
}
