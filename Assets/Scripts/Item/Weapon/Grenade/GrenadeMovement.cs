using UnityEngine;
using System.Collections;
using WeaponCharacter;

public class GrenadeMovement : ItemMovement
{
    Grenade grenade;
    private GrenadeInfoSO config;
    public override void Init(IItem newItem)
    {
        base.Init(newItem);
        grenade = (Grenade)newItem;
        config = (GrenadeInfoSO)(grenade.ItemComponents.InventoryItemInfo);

    }
    public override void UpdateMovement()
    {
        if(config.ExplodeOnColiision)
            base.CheckCollision();
    } 
}