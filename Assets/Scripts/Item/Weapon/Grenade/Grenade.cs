using UnityEngine;
using WeaponCharacter;

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider2D), typeof(SpriteRenderer))]
public class Grenade : BaseItem
{
    private GrenadeActions grenadeActions = new GrenadeActions();
    [SerializeField] private GrenadeComponents grenadeComponents = new GrenadeComponents();
    [SerializeField] private ItemStats grenadeStats = new ItemStats();
    protected GrenadeMovement grenadeMovement = new GrenadeMovement();
    protected ItemImpactPlaySound grenadeImpactPlaySound = new ItemImpactPlaySound();

    public override BaseItem Clone()
    {
        Grenade grenade = Instantiate<Grenade>(this);
        grenade.SetModules();
        return grenade;
    }
    public override void Init()
    {
        //component создается первым, потому что там конфиг
        SetModules();
        base.Init();
    }

    private void SetModules()
    {
        components = grenadeComponents;
        actions = grenadeActions;
        stats = grenadeStats;
        movement = grenadeMovement;
        itemImpactPlaySound = grenadeImpactPlaySound;
    }
}