using System.Collections;
using UnityEngine;
using WeaponCharacter;

public class GrenadeActions : AmmoActions
{
    private Grenade grenade;
    private GrenadeInfoSO config;

    public override void Init(BaseItem newItem)
    {
        grenade = (Grenade)newItem;
        config = (GrenadeInfoSO)(grenade.ItemComponents.InventoryItemInfo);
        base.Init(newItem);
    }
    public override void TryToThrowItem(Character character)
    {
        base.TryToThrowItem(character);
        grenade.StartCoroutine(BlastTimeout());
    }

    private IEnumerator BlastTimeout()
    {
        yield return new WaitForSeconds(config.WaitForSecondsToEsplode);
        BaseExplosion explosion = GameObject.Instantiate<BaseExplosion>(config.ExplosionOrigin, grenade.transform.position, Quaternion.identity);
        explosion.damageRadius = config.DamageRadius;
        explosion.damage = config.GrenadeDamage;
        GameObject.Destroy(grenade.gameObject);
    }
}