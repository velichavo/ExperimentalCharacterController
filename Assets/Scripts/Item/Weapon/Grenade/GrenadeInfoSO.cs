using UnityEngine;

namespace WeaponCharacter
{
    [CreateAssetMenu(fileName = "InventoryItemInfo", menuName = "Gameplay/Items/Create New Grernade")]
    public class GrenadeInfoSO : AmmoInfoSO
    {
        [SerializeField] private bool explodeOnColiision;
        [SerializeField] private int waitForSecondsToEsplode;
        [SerializeField] private float grenadeDamage;
        [SerializeField] private float damageRadius;
        [SerializeField] private BaseExplosion explosionPrefab;

        public float GrenadeDamage => grenadeDamage;
        public float DamageRadius => damageRadius;
        public bool ExplodeOnColiision => explodeOnColiision;
        public int WaitForSecondsToEsplode => waitForSecondsToEsplode;
        public BaseExplosion ExplosionOrigin => explosionPrefab;
    }
}