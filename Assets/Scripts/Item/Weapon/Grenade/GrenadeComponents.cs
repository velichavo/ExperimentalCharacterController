using InventoryCharacter;
using UnityEngine;

namespace WeaponCharacter
{
    [System.Serializable]
    public class GrenadeComponents : ItemComponents
    {
        [SerializeField] private GrenadeInfoSO sourceConfig;

        public override void Init(IItem newItem)
        {
            if (sourceConfig)
                inventoryItemInfo = sourceConfig;
            base.Init(newItem);
            baseCollider = item.ItemGameObject.GetComponent<CapsuleCollider2D>();
            SetItemInfo();
        }
        public override void SetItemInfo()
        {
            if(inventoryItemInfo != null && inventoryItem == null)
            {
                base.SetItemInfo();
                SetCollider();
                if (item.ItemStats.CurrentClipCapacity.Value <= 0)
                    item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = sourceConfig.maxItemsInInventorySlot;
                else
                    item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = item.ItemStats.CurrentClipCapacity.Value;
            }
        }
    }
}