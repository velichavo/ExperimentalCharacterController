using UnityEngine;
using InventoryCharacter;

namespace WeaponCharacter
{
    [CreateAssetMenu(fileName = "InventoryItemInfo", menuName = "Gameplay/Items/Create New Weapon")]
    public class WeaponInfoSO : InventoryItemInfo, IWeaponInfo
    {
        [SerializeField] private float reloadTime;
        [SerializeField] private float shootTime;
        [SerializeField] private float startShootTime;
        [SerializeField] private float endShootTime;
        [SerializeField] private int clipCapacity;
        [SerializeField] private float damage;
        [SerializeField] private float bulletSpeed;
        [SerializeField, Range(0, 100)] private float accuracy;
        [SerializeField, Range(1, 10)] private int bulletAmountPerShot = 1;
        [SerializeField] private bool isBurstShooting;
        [SerializeField] private bool isPlayReloadSoundEqualClipCapacity;
        [SerializeField] private Vector3 gripPositon;
        [SerializeField] private Vector3 handguardPosition;
        [SerializeField] private Vector3 spritePostion;
        [SerializeField] private float weaponShiftCoefficientWhenTurning;
        [SerializeField] private Vector2 offsetCollider;
        [SerializeField] private Sprite spriteWeaponNoClip;
        [SerializeField] private BaseBullet bullet;
        [SerializeField] private AudioClip shootSound;
        [SerializeField] private AudioClip reloadSound;
        [SerializeField] private AudioClip startShootSound;
        [SerializeField] private AudioClip endShootSound;
        [SerializeField] private AudioClip noAmmoSound;

        public float ReloadTime => reloadTime;
        public float ShootTime => shootTime;
        public float StartShootTime => startShootTime;
        public float EndShootTime => endShootTime;
        public int ClipCapacity => clipCapacity;
        public float Damage => damage;
        public float BulletSpeed => bulletSpeed;
        public float Accuracy => accuracy;
        public int BulletAmountPerShot => bulletAmountPerShot;
        public float WeaponShiftCoefficientWhenTurning => weaponShiftCoefficientWhenTurning;
        public bool IsBurstShooting => isBurstShooting;
        public bool IsPlayReloadSoundEqualClipCapacity => isPlayReloadSoundEqualClipCapacity;
        public Sprite SpriteWeaponNoClip => spriteWeaponNoClip;
        public Vector3 GripPositon => gripPositon;
        public Vector3 HandguardPosition => handguardPosition;
        public Vector3 SpritePostion => spritePostion;
        public Vector2 OffsetCollider => offsetCollider;
        public BaseBullet bulletInstance => bullet;
        public AudioClip ShootSound => shootSound;
        public AudioClip ReloadSound => reloadSound;
        public AudioClip StartShootSound => startShootSound;
        public AudioClip EndShootSound => endShootSound;
        public AudioClip NoAmmoSound => noAmmoSound;

        public override string ToString()
        {
            return base.ToString() + ($"ReloadTime: {reloadTime.ToString()}\nShootTime: {shootTime.ToString()}\nClipCapacity: {clipCapacity.ToString()}\nammoType: {AmmoType.ToString()}\n");
        }
    }
}