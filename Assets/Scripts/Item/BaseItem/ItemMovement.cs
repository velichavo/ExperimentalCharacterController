using UnityEngine;
using System.Collections;

public class ItemMovement
{
    IItem item;
    RaycastHit2D[] hit;
    public virtual void Init(IItem newItem)
    {
        item = newItem;
    }
    public virtual void UpdateMovement()
    {
        CheckCollision();
    } 

    protected void  CheckCollision()
    {
        hit = new RaycastHit2D[1];
        int count = item.ItemComponents.Rb2D.Cast(Vector2.zero, item.ItemComponents.Manager.LayerCollision, hit, 0);
        if (hit[0].collider != null)
        {
            IEntityCollisionAction bc = hit[0].transform.GetComponent<IEntityCollisionAction>();
            if (bc != null)
            {
                //На кидающего сила не применяется
                if(bc.OnItemCollision(item))
                    item.ItemComponents.Rb2D.velocity = Vector2.Reflect(item.ItemComponents.Rb2D.velocity, hit[0].normal);
            }
        }
    }

    public IEnumerator EndCollision()
    {
        item.ItemComponents.ItemAudioSource.PlayOneShot(item.ItemComponents.InventoryItemInfo.SoundThrow);
        yield return new WaitForSeconds(10);
        item.ItemComponents.Manager.OnThrowing -= UpdateMovement;
    }
}