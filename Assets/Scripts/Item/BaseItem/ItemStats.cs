using UnityEngine;


[System.Serializable]
public class ItemStats
{
    private IItem item;
    [SerializeField] private ObservedValue<int> currentClipCapacity = new ObservedValue<int>();
    public ObservedValue<int> CurrentClipCapacity {get => currentClipCapacity; set => currentClipCapacity = value;}

    public void Init(IItem newItem)
    {
        item = newItem;  
    }
}