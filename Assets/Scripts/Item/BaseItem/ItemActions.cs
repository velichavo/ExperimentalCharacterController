public abstract class ItemActions : IItemActions
{
    public abstract void Attack(Character character, GetKeyState state);

    public abstract BaseItem Equip(Character character, bool isEquipped, bool isThrowing = false);

    public abstract void Init(BaseItem newItem);

    public abstract void RotateInHand(Character character);

    public abstract void SetItemEnabled(bool enabled);

    public abstract void TryToThrowItem(Character character);
}