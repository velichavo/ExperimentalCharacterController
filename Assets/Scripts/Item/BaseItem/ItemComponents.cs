using InventoryCharacter;
using UnityEngine;

public abstract class ItemComponents : IItemComponents
{
    protected SpriteRenderer spriteRenderer;
    protected Rigidbody2D rb2D;
    protected Collider2D baseCollider;
    protected IInventoryItemInfo inventoryItemInfo;
    protected IItemComponents components;
    protected IItem item;
    protected AudioSource audioSource;
    private ItemManager manager;
    public GameObject parent {get; set;}
    public bool isEquiped {get; set;}
    public IInventoryItem inventoryItem {get; set;}
    public ICharacter character{get; set;}
    public Vector2 OldPosition {get; set;}
    public AudioSource ItemAudioSource => audioSource;
    public SpriteRenderer ItemSpriteRenderer
    {
        get => spriteRenderer;
        set => spriteRenderer = value;
    }
    public Rigidbody2D Rb2D
    {
        get => rb2D; 
        set => rb2D = value;
    }
    public Collider2D BaseCollider
    {
        get => baseCollider;
        set => baseCollider = value;
    }
    public IInventoryItemInfo InventoryItemInfo 
    {
        get 
        {
            return inventoryItemInfo;
        }
        set 
        { 
            inventoryItemInfo = value;
        }
    }
    
    public ItemManager Manager
    {
        get
        {
            if (!manager)
                manager = GameObject.FindObjectOfType<ItemManager>();
            return manager;        
        }
    }

    public virtual void SetItemInfo()
    {        
        inventoryItem = new InventoryItem(inventoryItemInfo, item);
        spriteRenderer.sprite = inventoryItemInfo.spriteItem;
        if (item.ItemComponents.inventoryItem.inventoryItemState.amount.Value == 0)
            item.ItemComponents.inventoryItem.inventoryItemState.amount.Value = item.ItemComponents.InventoryItemInfo.maxItemsInInventorySlot;
    }
    public virtual void Init(IItem newItem)
    {
        item = newItem;
        spriteRenderer = item.ItemGameObject.GetComponentInChildren<SpriteRenderer>();
        rb2D = item.ItemGameObject.GetComponent<Rigidbody2D>();
        audioSource = item.ItemGameObject.GetComponent<AudioSource>();
    }

    protected void SetCollider()
    {
        switch (inventoryItemInfo.ColliderItemTypeInfo)
        {
            case ColliderItemType.PolygonType:
                ((PolygonCollider2D)baseCollider).SetPath(0, inventoryItemInfo.ColliderPoints);
                break;
            case ColliderItemType.CapsuleType:
                if(inventoryItemInfo.ColliderPoints.Length == 3)
                {
                    ((CapsuleCollider2D)baseCollider).offset = inventoryItemInfo.ColliderPoints[0];
                    ((CapsuleCollider2D)baseCollider).size = inventoryItemInfo.ColliderPoints[1];
                    ((CapsuleCollider2D)baseCollider).direction = (CapsuleDirection2D)(inventoryItemInfo.ColliderPoints[2].x);
                }
                else
                {
                    throw new System.Exception("ColliderPoints must contain at least 3 elements");
                }
                break;
            case ColliderItemType.BoxType:
                if(inventoryItemInfo.ColliderPoints.Length == 2)
                {
                    ((BoxCollider2D)baseCollider).offset = inventoryItemInfo.ColliderPoints[0];
                    ((BoxCollider2D)baseCollider).size = inventoryItemInfo.ColliderPoints[1];
                }
                else
                {
                    throw new System.Exception("ColliderPoints must contain at least 2 elements");
                }
                break;
        }
    }
}