using UnityEngine;
using System;

public class BaseItem : MonoBehaviour, IItem, IInfoViewerble
{
    protected IItemComponents components;
    protected IItemActions actions;
    protected ItemStats stats;
    protected ItemMovement movement;
    protected ItemImpactPlaySound itemImpactPlaySound;
    public IItemComponents ItemComponents => components;
    public IItemActions ItemActions => actions;
    public ItemStats ItemStats => stats;
    public ItemMovement ItemMovement => movement;
    public ItemImpactPlaySound ImpactPlaySound => itemImpactPlaySound;
    public GameObject ItemGameObject {get; private set;}

    protected virtual void Awake()
    {
        Init();
    }
    protected void OnCollisionEnter2D(Collision2D collision)
    {
        IEntityCollision collisional = collision.transform.GetComponent<IEntityCollision>();
        
        if (collisional != null)
            itemImpactPlaySound.PlaySound(collisional);
    }
    public virtual BaseItem Clone()
    {
        return Instantiate<BaseItem>(this);
    }
    public virtual void Init()
    {
        if(components.inventoryItem != null) return;
        
        ItemGameObject = gameObject;
        components.Init(this);
        actions.Init(this);   
        stats.Init(this);
        movement.Init(this);
        itemImpactPlaySound.Init(this);
    }
    public virtual void Destroy()
    {
        stats.CurrentClipCapacity.OnChanged -= components.inventoryItem.ChangeTextUIClip;
        components.inventoryItem.inventoryItemState.amount.OnChanged -= components.inventoryItem.ChangeTextUIAmmo;
        Destroy(gameObject);
    }

    public virtual void ViewInfo(bool enabled)
    {
        
    }

    public virtual void TakeActionOnItem(Character other)
    {
        other.Actions.CheckCollisionItem();
    }
}
