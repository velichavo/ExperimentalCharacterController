public class ItemImpactPlaySound
{
    BaseItem item;

    public void Init(BaseItem newItem)
    {
        item = newItem;
    }
    public void PlaySound(IEntityCollision collisional)
    {
        item.ItemComponents.ItemAudioSource.PlayOneShot(item.ItemComponents.InventoryItemInfo.SoundImpact[(int)collisional.SurfaceType]);
    }
}