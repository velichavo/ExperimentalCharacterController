using UnityEngine;

namespace WeaponCharacter
{
    interface IWeaponInfo
    {
        float ReloadTime {get;}
        float ShootTime {get;}
        float StartShootTime {get;}
        float EndShootTime {get;}
        int ClipCapacity {get;}
        float Damage {get;}
        float BulletSpeed {get;}
        float WeaponShiftCoefficientWhenTurning {get;}
        bool IsBurstShooting {get;}
        bool IsPlayReloadSoundEqualClipCapacity {get;}
        Sprite SpriteWeaponNoClip {get;}
        Vector3 GripPositon {get;}
        Vector3 HandguardPosition {get;}
        Vector3 SpritePostion {get;}
        Vector2 OffsetCollider {get;}
        BaseBullet bulletInstance {get;}
        AudioClip ShootSound {get;}
        AudioClip ReloadSound {get;}
        AudioClip StartShootSound {get;}
        AudioClip EndShootSound {get;}
        AudioClip NoAmmoSound {get;}
    }
}