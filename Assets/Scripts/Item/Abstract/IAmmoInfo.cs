using UnityEngine;

namespace WeaponCharacter
{
    public interface IAmmoInfo
    {
        float DamageMultiplier {get;}
        string AmmoTypeText {get;}
        Sprite SpriteAmmo {get;}
        public Vector2 OffsetCollider {get;}
    }
}