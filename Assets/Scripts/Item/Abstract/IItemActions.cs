using System;

public interface IItemActions
{
    //event Action<IItem> OnChangeItemAmount;
    void Init(BaseItem newItem);
    BaseItem Equip(Character character, bool isEquipped, bool isThrowing = false);
    void SetItemEnabled(bool enabled);
    void Attack(Character character, GetKeyState state);
    void RotateInHand(Character character);
    void TryToThrowItem(Character character);
}