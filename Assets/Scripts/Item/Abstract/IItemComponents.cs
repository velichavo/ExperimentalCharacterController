using UnityEngine;

public interface IItemComponents
{
    GameObject parent {get; set;}
    ICharacter character {get; set;}
    IInventoryItem inventoryItem {get; set;}
    SpriteRenderer ItemSpriteRenderer {get; set;}
    Rigidbody2D Rb2D{get; set;}
    Collider2D BaseCollider{get; set;}
    IInventoryItemInfo InventoryItemInfo{get; set;}
    Vector2 OldPosition {get; set;}
    ItemManager Manager {get;}
    AudioSource ItemAudioSource {get;}
    void Init(IItem item);
    void SetItemInfo();
}