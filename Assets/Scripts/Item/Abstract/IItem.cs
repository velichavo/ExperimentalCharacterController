using System;
using UnityEngine;
using WeaponCharacter;

public interface IItem
{
    IItemActions ItemActions {get;}
    IItemComponents ItemComponents {get;}
    ItemStats ItemStats {get;}
    ItemMovement ItemMovement {get;}
    GameObject ItemGameObject{ get;}
    BaseItem Clone();
    void Init();
    void Destroy();
}