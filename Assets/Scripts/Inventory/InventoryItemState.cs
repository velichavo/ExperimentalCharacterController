using UnityEngine;
using UnityEngine.UI;

namespace InventoryCharacter
{
    [System.Serializable]
    public class InventoryItemState : IInventoryItemState
    {
        [SerializeField] private ObservedValue<int> itemAmount;
        [SerializeField] private bool isItemEquipped;
        public bool isEquipped { get => isItemEquipped; set => isItemEquipped = value; }
        public ObservedValue<int> amount { get => itemAmount; set => itemAmount = value; }
        public Toggle toggle{get; set;}

        public InventoryItemState()
        {
            itemAmount = new ObservedValue<int>(0);
            isItemEquipped = false;
        }
    }
}