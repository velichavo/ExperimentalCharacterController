using System;
using UnityEngine;
using WeaponCharacter;

namespace InventoryCharacter
{
    [Serializable]
    public class InventoryItem : IInventoryItem
    {
        [SerializeField] private InventoryItemState stt;
        protected IInventoryItemInfo info;
        protected IInventoryItemState state;
        protected IItem item;

        public IInventoryItemInfo inventoryItemInfo => info;
        public IInventoryItemState inventoryItemState 
        {
            get
            {
                stt = (InventoryItemState)state;
                return state;
            }
        }
        public Type type => GetType();
        public IItem Item => item;

        public InventoryItem(IInventoryItemInfo newInfo, IItem newItem)
        {
            item = newItem;
            info = newInfo;
            state = new InventoryItemState();
        }
        public void Equip(bool isEquipped)
        {
            state.isEquipped = isEquipped;
            //переделать вызов напрямую в оружие
            item.ItemComponents.character.Actions.Equip(item, isEquipped);
            //подписываем на обновление UI
            NewPlayer player = item.ItemComponents.character as NewPlayer;
            if(player)
                SetSubscribe(player, isEquipped);
        }
        public IInventoryItem Clone()
        {
            return GameObject.Instantiate(item.ItemGameObject, item.ItemGameObject.transform.position, item.ItemGameObject.transform.rotation).GetComponent<IInventoryItem>();
        }

        public void ChangeTextUIClip(object value)
        {   
            NewPlayer player = item.ItemComponents.character as NewPlayer;
            player.Utilities.ChangeTextUIClip((int)value);
        }
        public void ChangeTextUIAmmo(object value)
        {
            NewPlayer player = item.ItemComponents.character as NewPlayer;
            player.Utilities.ChangeTextUIAmmo((int)value);
        }

        private void SetSubscribe(NewPlayer player, bool isEquipped)
        {
            if (isEquipped)
            {
                item.ItemStats.CurrentClipCapacity.OnChanged += ChangeTextUIClip;
                player.Utilities.ChangeTextUIClip(item.ItemStats.CurrentClipCapacity.Value);

                IInventoryItem ammo = GetAmmoItem(player);
                if (ammo != null)
                {
                    ammo.inventoryItemState.amount.OnChanged += ChangeTextUIAmmo;
                    player.Utilities.ChangeTextUIAmmo(ammo.inventoryItemState.amount.Value);
                }
            }
            else
            {
                item.ItemStats.CurrentClipCapacity.OnChanged -= ChangeTextUIClip;
                player.Utilities.ChangeTextUIClip(-1);

                IInventoryItem ammo = GetAmmoItem(player);
                if (ammo != null)
                {
                    ammo.inventoryItemState.amount.OnChanged -= ChangeTextUIAmmo;
                    player.Utilities.ChangeTextUIAmmo(-1);
                }
            }
        }

        private IInventoryItem GetAmmoItem(NewPlayer player)
        {
            InventoryItemInfo info = item.ItemComponents.InventoryItemInfo.AmmoType;
            return player.ObjectReferences.CurrentInventory.GetItem(info);
        }
    }
}