using InventoryCharacter;
using UnityEngine;

[System.Serializable]
public class InventoryItemInInspector
{
    [SerializeField] InventoryItemInfo characterItemInfo;
    [SerializeField] int amount;

    public InventoryItemInfo CharacterItemInfo => characterItemInfo;
    public int Amount => amount;
}