using System;

public interface IInventorySlot
{
    bool isFull {get;}
    bool isEmpty {get;}

    IInventoryItem item {get;}
    Type itemType {get;}
    int amount {get;}
    int capacity {get;}
    void SetItem(IInventoryItem item, int amount);
    int RemoveItem(IInventoryItem item, int amount = 1);
    void Clear();
}