using System;

public interface IInventoryItem
{
    IInventoryItemInfo inventoryItemInfo { get; }
    IInventoryItemState inventoryItemState { get; }
    IItem Item {get;}
    Type type { get; }

    void Equip(bool equip);    
    IInventoryItem Clone();
    void ChangeTextUIClip(object value);
    void ChangeTextUIAmmo(object value);

}
