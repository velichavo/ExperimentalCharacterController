using UnityEngine;
using InventoryCharacter;
using WeaponCharacter;

public interface IInventoryItemInfo
{
    string id { get; }
    string title { get; }
    string description { get; }
    float weight { get; }
    bool isCanEquip {get;}
    bool eachInNewSlot {get;}
    int maxItemsInInventorySlot { get; }
    Sprite icon{ get; }
    Sprite spriteItem { get; }
    GameObject Prefab {get;}
    InventoryItemInfo ItemType {get;}
    AmmoInfoSO AmmoType {get;}
    ColliderItemType ColliderItemTypeInfo{get;}
    Vector2[] ColliderPoints {get;}
    AudioClip SoundThrow{get;}
    AudioClip[] SoundImpact{get;}
}