using UnityEngine.UI;

public interface IInventoryItemState
{
    bool isEquipped { get; set; }
    ObservedValue<int> amount { get; set; }
    Toggle toggle {get; set;}
    
}