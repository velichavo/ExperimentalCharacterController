using System;

public interface IInventory
{
    int capacity { get; set; }
    bool isFull { get; }

    IInventoryItem GetItem(IInventoryItemInfo itemType);
    IInventoryItem[] GetAllItems();
    IInventoryItem[] GetAllItems(Type itemType);
    IInventoryItem[] GetEquipmentItems();
    int GetItemAmount(IInventoryItemInfo itemType);

    bool TryToAdd(object sender, IInventoryItem item, int amount);
    int Remove(object sender, IInventoryItem item, int amount = 1);

    bool HasItem(Type itemType, out IInventoryItem item);

}