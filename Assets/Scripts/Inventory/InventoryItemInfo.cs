using UnityEngine;
using WeaponCharacter;

namespace InventoryCharacter
{
    public enum ItemTypeInfo {Default = 0, Weapon}
    public enum ColliderItemType {none, PolygonType, CapsuleType, BoxType}

    [CreateAssetMenu(fileName = "InventoryItemInfo", menuName = "Gameplay/Items/Create New ItemInfo")]
    public class InventoryItemInfo : ScriptableObject, IInventoryItemInfo
    {
        [SerializeField] private string idInfo;
        [SerializeField] private string titleInfo;
        [SerializeField] private string descriptionInfo;
        [SerializeField] private float weightInfo;
        [SerializeField] private bool isCanEquipInfo;
        [SerializeField] private bool eachInNewSlotInfo;
        [SerializeField] private int maxItemsInInventorySlotInfo;
        [SerializeField] private Sprite iconInfo;
        [SerializeField] private Sprite spriteItemInfo;
        [SerializeField] private InventoryItemInfo itemTypeInfo;
        [SerializeField] private AmmoInfoSO ammoTypeInfo;
        [SerializeField] private ColliderItemType colliderItemTypeInfo;
        [SerializeField] private Vector2[] colliderPoints;
        [SerializeField] private GameObject prefab;
        [SerializeField] private AudioClip soundThrow;
        [SerializeField] private AudioClip[] soundImpact = new AudioClip[10];

        public string id => idInfo;
        public string title => titleInfo;
        public string description => descriptionInfo;
        public int maxItemsInInventorySlot => maxItemsInInventorySlotInfo;
        public bool eachInNewSlot => eachInNewSlotInfo;
        public bool isCanEquip => isCanEquipInfo;
        public Sprite icon => iconInfo;
        public Sprite spriteItem => spriteItemInfo;
        public InventoryItemInfo ItemType => itemTypeInfo;
        public AmmoInfoSO AmmoType => ammoTypeInfo;
        public float weight => weightInfo;
        public GameObject Prefab => prefab;
        public Vector2[] ColliderPoints => colliderPoints;
        public ColliderItemType ColliderItemTypeInfo => colliderItemTypeInfo;
        public AudioClip SoundThrow => soundThrow;
        public AudioClip[] SoundImpact => soundImpact;

        public override string ToString()
        {
            return ($"Id: {idInfo}\nTitle: {titleInfo}\nDescription: {descriptionInfo}\nMaxItemsInInventorySlot: {maxItemsInInventorySlotInfo.ToString()}\nIcon: {iconInfo.ToString()}\nitemType: {ammoTypeInfo.GetType()}\n");
        }
    }
}