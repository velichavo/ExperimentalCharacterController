using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WeaponCharacter;

namespace InventoryCharacter
{
    [Serializable]
    public class InventoryWithSlots : IInventory
    {
        public event Action<object, InventoryWithSlots> OnInventoryItemRefreshEvent;

        private List<IInventorySlot> inventorySlots;

        public Character character {get; private set;}
        public int capacity { get; set; }
        public bool isFull => inventorySlots.All(slot => slot.isFull);

        public InventoryWithSlots(Character newCharacter, int capacity = 1)
        {
            this.capacity = capacity;
            inventorySlots = new List<IInventorySlot>();
            character = newCharacter;
        }
        public void AddItemOnStart(object sender, InventoryItemInInspector[] inventoryItemInInspectors)
        {
            if (inventoryItemInInspectors == null) return;
            foreach (InventoryItemInInspector item in inventoryItemInInspectors)
            {
                IItem itm = CreateItem(sender, item.CharacterItemInfo);
                TryToAdd(sender, itm.ItemComponents.inventoryItem, item.Amount);
            }
        }

        public IItem CreateItem(object sender, IInventoryItemInfo inventoryItemInfo)
        {
            IItem itm = MonoBehaviour.Instantiate<GameObject>(inventoryItemInfo.Prefab).GetComponent<IItem>();
            itm.ItemComponents.InventoryItemInfo = inventoryItemInfo;
            itm.ItemComponents.SetItemInfo();
            itm.ItemActions.SetItemEnabled(false);
            itm.ItemGameObject.transform.SetParent(itm.ItemComponents.Manager.transform);
            return itm;
        }
        public IInventoryItem[] GetAllItems()
        {
            List<IInventoryItem> allItems = new List<IInventoryItem>();
            foreach(IInventorySlot slot in inventorySlots)
            {
                if(!slot.isEmpty)
                    allItems.Add(slot.item);
            }
            return allItems.ToArray();
        }

        public IInventoryItem[] GetAllItems(Type itemType)
        {
            List<IInventoryItem> allItems = new List<IInventoryItem>();
            foreach(IInventorySlot slot in inventorySlots)
            {
                if(slot.itemType == itemType)
                    allItems.Add(slot.item);
            }
            return allItems.ToArray();
        }

        public IInventoryItem[] GetEquipmentItems()
        {
             List<IInventoryItem> equipedItems = new List<IInventoryItem>();
            foreach(IInventorySlot slot in inventorySlots)
            {
                if(slot.item.inventoryItemState.isEquipped)
                    equipedItems.Add(slot.item);
            }
            return equipedItems.ToArray();
        }

        public IInventoryItem GetItem(IInventoryItemInfo itemType)
        {
            if (itemType == null) return null;
            IInventorySlot slot = inventorySlots.Find(slot => slot.item.inventoryItemInfo.id == itemType.id);
            return slot != null ? slot.item : null;
        }

        public int GetItemAmount(IInventoryItemInfo itemType)
        {
            IInventorySlot slot = inventorySlots.Find(slot => slot.item.inventoryItemInfo.id == itemType.id);
            return slot != null ? slot.item.inventoryItemState.amount.Value : 0;
        }

        public bool HasItem(Type itemType, out IInventoryItem item)
        {
            throw new NotImplementedException();
        }

        public int Remove(object sender, IInventoryItem item, int amount = 1)
        {
            int remainder = 0;
            //item.Item.ItemComponents.character = null;
            foreach(IInventorySlot slot in inventorySlots)
            {
                if(item.inventoryItemInfo.id == slot.item.inventoryItemInfo.id)
                {
                    //Если больше или равно, то удаляем слот и возвращаем остаток.
                    remainder = slot.RemoveItem(item, amount);
                    //Debug.Log(slot.item.inventoryItemState.amount);
                    if (slot.item.inventoryItemState.amount.Value <= 0)
                    {
                        inventorySlots.Remove(slot);
                        break;
                    }
                }
            }
            OnInventoryItemRefreshEvent?.Invoke(sender, this);
            return remainder;
        }

        public void AddItem(object sender, IInventoryItem item, int amount = -1)
        {
            TryToAdd(sender, item, amount);
        }

        public bool TryToAdd(object sender, IInventoryItem item, int amount = -1)
        {
            foreach (IInventorySlot slot in inventorySlots)
            {
                if(item.inventoryItemInfo.id == slot.item.inventoryItemInfo.id && !item.inventoryItemInfo.eachInNewSlot)
                {
                    slot.SetItem(item, amount);
                    OnInventoryItemRefreshEvent?.Invoke(sender, this);
                    return true;
                }
            }
            InventorySlot newInventorySlot = new InventorySlot();
            inventorySlots.Add(newInventorySlot);
            newInventorySlot.SetItem(item, amount);
            item.Item.ItemComponents.character = (ICharacter)sender;
            OnInventoryItemRefreshEvent?.Invoke(sender, this);
            return true;
        }

        public override string ToString()
        {
            string text = "";
            foreach (IInventorySlot slot in inventorySlots)
            {
                text += ($"{slot.item.inventoryItemInfo.title}     {slot.amount}\n");
            }
            if (text == "") text = "empty";
            return text;
        }
    }
}