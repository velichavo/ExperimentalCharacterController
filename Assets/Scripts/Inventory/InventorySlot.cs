using System;
using UnityEngine;

namespace InventoryCharacter
{
    public class InventorySlot : IInventorySlot
    {
        public bool isFull => amount == capacity;

        public bool isEmpty => item == null;

        public IInventoryItem item {get; private set;}

        public Type itemType => item.type;

        public int amount => isEmpty ? 0 : item.inventoryItemState.amount.Value;

        public int capacity {get; private set;}

        public void Clear()
        {
            if(!isEmpty) return;
            item.inventoryItemState.amount.Value = 0;
            item = null;
            
        }
        ///<summary>
        /// Если передается -1, то количество берется из scriptableObject
        ///<summary>
        public void SetItem(IInventoryItem item, int amount)
        {
            if(isEmpty)
            {
                this.item = item;
                if(amount > 0)
                    item.inventoryItemState.amount.Value = amount;
            }
            else
                if(this.item != item)
                {
                    item.Item.Destroy();
            
                    this.item.inventoryItemState.amount.Value += amount == -1 ? item.inventoryItemState.amount.Value : amount;
                }
        }
        //Остается 0, то удаляем слот
        public int RemoveItem(IInventoryItem item, int amount = 1)
        {
            if(this.item.inventoryItemState.amount.Value > amount)
            {
                this.item.inventoryItemState.amount.Value -= amount;
                return amount;
            }
            int remainder = this.item.inventoryItemState.amount.Value;
            this.item.inventoryItemState.amount.Value = 0;
            this.item.Item.Destroy();
            return remainder;
        }
    }
}