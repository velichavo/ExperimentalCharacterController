using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RIG {BODY, LEGS};
public class AnyStateAnimation
{
    public RIG AnimationRig {get; private set;}
    public bool Active { get; set; }
    public AnimationVariables[] HigherPrio{get; private set;}
    public string Name{get; private set;}
    public AnimationVariables AnimationSwitch{get; private set;}
    public AnyStateAnimation(RIG rig, string name, AnimationVariables animationSwitch, params AnimationVariables[] higherPrio)
    {
        AnimationRig = rig;
        Name = name;
        HigherPrio = higherPrio;
        AnimationSwitch = animationSwitch;
    }
    
}
