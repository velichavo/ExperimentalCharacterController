using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnyStateAnimator : MonoBehaviour
{
    [SerializeField] private Animator animator;
    private Character character;
    private Dictionary<AnimationVariables, AnyStateAnimation> animations = new Dictionary<AnimationVariables, AnyStateAnimation>();
    private AnimationVariables currentAnimationLegs;
    private AnimationVariables currentAnimationBody;

    public AnimationVariables CurrentAnimationLegs => currentAnimationLegs;
    public AnimationVariables CurrentAnimationBody => currentAnimationBody;
    public void Init(Character newCharacter)
    {
        animator = GetComponent<Animator>();
        character = newCharacter;
    }
    private void Update()
    {
        Animate();
    }
    public void AddAnimations(params AnyStateAnimation[] newAnimations)
    {
        for (int i = 0; i < newAnimations.Length; i++)
        {
            animations.Add(newAnimations[i].AnimationSwitch, newAnimations[i]);
        }
    }
    public void TryPlayAnimation(AnimationVariables newAnimation)
    {
        switch (animations[newAnimation].AnimationRig)
        {
            case RIG.BODY:
                PlayAnimation(ref currentAnimationBody);
                break;
            case RIG.LEGS:
                PlayAnimation(ref currentAnimationLegs);
                break;
        }
        void PlayAnimation(ref AnimationVariables currentAnimation)
        {
            if(currentAnimation == AnimationVariables.none)
            {
                animations[newAnimation].Active = true;
                currentAnimation = newAnimation;
            }
            else if(currentAnimation != newAnimation && !animations[newAnimation].HigherPrio.Contains(currentAnimation) || !animations[currentAnimation].Active)
            {
                animations[currentAnimation].Active = false;
                animations[newAnimation].Active = true;
                currentAnimation = newAnimation;
            }
        }
    }
    private void Animate()
    {
        foreach (AnimationVariables item in animations.Keys)
        {
            animator.SetBool(animations[item].Name, animations[item].Active); 
        }
    }

    public void OnAnimationEnd(AnimationVariables newAnimation)
    {
        animations[newAnimation].Active = false;
    }
    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
        animator.enabled = enabled;
    }

    public void TryJump()
    {
        character.Actions.Jump();
    }

    public void TryEndClimbing()
    {
        character.ActionClimbing.EndClimbing();
    }
    public void SetCaptureTarget()
    {
        character.Actions.SetCaptureTarget(true);
    }
    public void RemoveCaptureTarget()
    {
        character.Actions.SetCaptureTarget(false);
    }

    public void EndSwing()
    {
        character.ActionThrow.EndSwing();
    }
    public void EndThrowing()
    {
        character.ActionThrow.EndThrowing();
    }
    public void Throw()
    {
        character.ObjectReferences.currentWeapon.ItemActions.TryToThrowItem(character);
    }
}

public enum AnimationVariables 
{
    none,
    body_idle,
    body_walk_forward,
    body_walk_back,
    body_jump_forward,
    body_jump,
    body_slow_run_forward,
    body_slow_run_back,
    body_climbing,
    body_climbing_middle,
    body_throw_item_begin,
    body_throw_item_end,

    legs_idle,
    legs_walk_forward,
    legs_walk_back,
    legs_jump_forward,
    legs_jump,
    legs_slow_run_forward,
    legs_slow_run_back,
    legs_climbing,
    legs_climbing_middle
}
