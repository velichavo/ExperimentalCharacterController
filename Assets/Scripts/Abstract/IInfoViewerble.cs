using UnityEngine;

public interface IInfoViewerble
{
    void ViewInfo(bool enabled);
    void TakeActionOnItem(Character other);
}