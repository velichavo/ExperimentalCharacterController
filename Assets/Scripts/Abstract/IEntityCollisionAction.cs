using UnityEngine;

public interface IEntityCollisionAction : IEntityCollision
{
    GameObject CharacterParent{get;}
    void OnBulletCollision(BaseBullet bullet);
    bool OnItemCollision(IItem item);
    void OnItemExplosion(BaseExplosion explosion);
    void ApplyForceEntity(BaseExplosion explosion);
}