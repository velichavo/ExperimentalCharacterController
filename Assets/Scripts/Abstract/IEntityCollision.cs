using UnityEngine;

public enum ItemCollisionSurfaceType {Concrete, Dirt, Glass, Grass, Metal, Sand, Snow, Stones, Wood, Charecter}
public interface IEntityCollision
{
    ItemCollisionSurfaceType SurfaceType{get;}
}
