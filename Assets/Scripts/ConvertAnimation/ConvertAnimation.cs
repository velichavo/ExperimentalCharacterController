using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class ConvertAnimation : MonoBehaviour
{
    [SerializeField] public AnimationClip clip;
    [SerializeField] public AnimationClip targetClip;
    public Transform RootBone;
    [SerializeField] public Transform testTransform;
    [SerializeField] private Transform axisTransform;
    public SourceBones[] SourceBone;
    public Transform[] targetBone;
    public Transform[] SourceBoneTransform;
    public Vector3[] intermediateBones = new Vector3[7];

    public Quaternion Rot => Quaternion.LookRotation(Vector3.forward, Vector3.up); 
    public Transform AxisTransform {get => axisTransform; set => axisTransform = value;}
    private void Start()
    {
        //print(Quaternion.Dot(testTransform.rotation, Quaternion.identity));
    }
    /* private void OnDrawGizmos()
    {
        Gizmos.DrawRay(SourceBoneTransform[0].position + Vector3.up, intermediateBones[0]);
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(SourceBoneTransform[1].position + Vector3.up, intermediateBones[1]);
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(SourceBoneTransform[2].position + Vector3.up, intermediateBones[2]);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(SourceBoneTransform[3].position + Vector3.up, intermediateBones[3]);
    } */
}

