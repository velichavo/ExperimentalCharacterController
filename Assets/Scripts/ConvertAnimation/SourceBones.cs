    using UnityEngine;
    
    [System.Serializable]
    public class SourceBones
    {
        public Transform[] Bones;
        public int[] Positions;
    }