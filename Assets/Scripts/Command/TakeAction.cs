using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeAction : Command
{
    public TakeAction(Character newCharacter, KeyCode key, bool activeInInterface = false) : base(newCharacter, key)
    {

    }
    public override void GetKeyDown()
    {
        character.Actions.CheckCollisionItem();
    }
}
