using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadWeaponCommands : Command
{
    public ReloadWeaponCommands(Character newCharacter, KeyCode key, bool activeInInterface = false) : base(newCharacter, key)
    {
    }
    public override void GetKeyDown()
    {
        character.Actions.ReloadWeapon();
    }
}
