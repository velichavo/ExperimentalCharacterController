using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCommand : Command
{
    public JumpCommand(Character newCharacter, KeyCode key, bool activeInInterface = false) : base(newCharacter, key)
    {
    }
    public override void GetKeyDown()
    {
        character.Actions.BeginJump();
    }
}
