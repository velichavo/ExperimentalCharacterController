using UnityEngine;

public abstract class Command
{
    protected Character character;
    public bool ActiveInInterface {get; private set;}
    public KeyCode Key {get; private set;}
    public Command(Character newCharacter, KeyCode key, bool activeInInterface = false)
    {
        character = newCharacter;
        Key = key;
        ActiveInInterface = activeInInterface;
    }
    public virtual void GetKeyDown()
    {

    }
    public virtual void GetKeyUp()
    {
        
    }
    public virtual void GetKey()
    {
        
    }
}
