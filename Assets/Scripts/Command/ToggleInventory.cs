using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInventory : Command
{
    public ToggleInventory(Character newCharacter, KeyCode key, bool activeInInterface = false) : base(newCharacter, key, activeInInterface)
    {

    }
    public override void GetKeyDown()
    {
        ((NewPlayer)character).Utilities.PlayerUIController.ToggleInventory();
    }
}
