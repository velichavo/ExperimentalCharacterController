using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GetKeyState {GetKey, GetKeyDown, GetKeyUp}
public class AttackCommand : Command
{
    public AttackCommand(Character newCharacter, KeyCode key, bool activeInInterface = false) : base(newCharacter, key)
    {
    }
    public override void GetKeyDown()
    {
        character.Actions.Attack(GetKeyState.GetKeyDown);
    }
    public override void GetKeyUp()
    {
        character.Actions.Attack(GetKeyState.GetKeyUp);
    }
    public override void GetKey()
    {
        character.Actions.Attack(GetKeyState.GetKey);
    }
}
